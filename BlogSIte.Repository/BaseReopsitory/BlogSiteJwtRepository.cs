﻿using BlogSite.Database.Mysql;
using BlogSite.Entity.Configs;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;

namespace BlogSite.Repository.BaseRepository
{
    public class BlogSiteJwtRepository:BlogSiteRepository
    {
        public JwtSecurityTokenHandler TokenHandler { get; protected set; }
        public JwtTokenSettingConfig JwtTokenSettingConfig { get; protected set; }

        public BlogSiteJwtRepository(ILogger logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtTokenSettingConfig, JwtSecurityTokenHandler tokenHandler)
            : base(logger, mysqlDbContext, distributedCache)
        {
            TokenHandler = tokenHandler;
            JwtTokenSettingConfig = jwtTokenSettingConfig.Value;
        }
    }
}
