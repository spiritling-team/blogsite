﻿using BlogSite.CommonLib.CommonEntity;
using BlogSite.CommonLib.Extension;
using BlogSite.Database.Mysql;
using BlogSite.Entity.ArticleTag;
using BlogSite.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSite.Repository
{
    public class ArticleTagRepository : BlogSiteRepository
    {
        #region Constructor
        public ArticleTagRepository(ILogger logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache)
            : base(logger, blogSiteDbContext, distributedCache)
        {
        }
        #endregion

        #region Public Method
        public async Task<bool> ValidatePostDataIsExistAsync(string articleTagName)
        {
            var result = await (from p in DbContext.ArticleTag
                                where p.Name == articleTagName.Trim()
                                select p).FirstOrDefaultAsync();
            return result != null;
        }

        public async Task<bool> InsertData2MysqlAsync(ArticleTagCreatePostInput articleTagPostInput)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleTag = new ArticleTag
                {
                    Name = articleTagPostInput.Name,
                    BgColor = articleTagPostInput.BgColor,
                    TextColor = articleTagPostInput.TextColor,
                    UrlParam = articleTagPostInput.UrlParam,
                    Remark = articleTagPostInput.Remark,
                    KeyId = Guid.NewGuid().ToString(),
                    CreatedTime = DateTime.Now,
                };
                await DbContext.ArticleTag.AddAsync(articleTag);
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                Logger.LogError(e, "ArticleTagRepository Insert ArticleTag Create Post");
                return false;
            }
            finally
            {
                await t.DisposeAsync();
            }
        }

        public async Task<bool> Delete4IdAsync(int tagId)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleTag = await (from p in DbContext.ArticleTag
                                        where p.Id == tagId
                                        select p).FirstOrDefaultAsync();

                DbContext.ArticleTag.Remove(articleTag);
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                throw new Exception($"ArticleTagRepository Delete ArticleTag {tagId}",e);
            }
            finally
            {
                await t.DisposeAsync();
            }
        }
        public async Task<bool> EditData2MysqlAsync(ArticleTagEditPutInput articleTagEditPutInput)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleTag = await (from p in DbContext.ArticleTag
                                        where p.Id == articleTagEditPutInput.Id
                                        select p).FirstOrDefaultAsync();
                articleTag.Name = articleTagEditPutInput.Name;
                articleTag.BgColor = articleTagEditPutInput.BgColor;
                articleTag.TextColor = articleTagEditPutInput.TextColor;
                articleTag.Remark = articleTagEditPutInput.Remark;
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                Logger.LogError(e, "ArticleTagRepository Update ArticleTag Edit Put");
                return false;
            }
            finally
            {
                await t.DisposeAsync();
            }
        }

        public async Task<ArticleTag> FindFirstArticle4IdAsync(int tagId)
        {
            return await (from p in DbContext.ArticleTag
                          where p.Id == tagId
                          select p).FirstOrDefaultAsync();
        }
        public async Task<ResultListEntity<ArticleTag>> GetListAsync(CoditionEntity coditionEntity)
        {
            var query = (from p in DbContext.ArticleTag
                         select p).AsQueryable();
            var findResult = query.UseCoditionFind(coditionEntity);
            var count = await findResult.Item1.CountAsync();
            coditionEntity.PaginationKeyValue.Count = count;
            var dataList = await findResult.Item2.ToListAsync();
            var result = new ResultListEntity<ArticleTag>()
            {
                CoditionEntity = coditionEntity,
                Data = dataList
            };
            return result;
        }
        #endregion
    }
}
