﻿using BlogSite.Database.Mysql;
using BlogSite.Entity.Configs;
using BlogSite.Entity.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using BlogSite.Repository.BaseRepository;
using BlogSite.Database.Redis;

namespace BlogSite.Repository
{
    public class UserRepository : BlogSiteJwtRepository
    {
        #region Constructor
        public UserRepository(ILogger logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtTokenSettingConfig, JwtSecurityTokenHandler tokenHandle)
            : base(logger, blogSiteDbContext, distributedCache, jwtTokenSettingConfig, tokenHandle)
        {
        }
        #endregion

        #region Public Method
        /// <summary>
        /// 通过用户名字查询用户是否存在
        /// </summary>
        public async Task<string> FindUserSalt4NameAsync(string userName)
        {
            return await (from p in DbContext.User
                           where p.UserName == userName
                           select p.Salt).FirstOrDefaultAsync();
        }
        /// <summary>
        /// 查询用户账户和密码是否正确
        /// </summary>
        public async Task<User> FindUserInfo4LoginAsync(LoginInput loginInput)
        {
            return await (from p in DbContext.User
                          where p.UserName == loginInput.UserName && p.PassWord == loginInput.PassWord
                          select p).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<bool> InsertRefreshTokenAsync(JwtTokenOutput jwtTokenOutput)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var user = await (from p in DbContext.User
                                  where p.KeyId.Equals(jwtTokenOutput.UserKeyId)
                                  select p).FirstOrDefaultAsync();
                user.RefreshToken = jwtTokenOutput.RefreshToken.ToString();
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                Logger.LogError(e, "UserBusiness InsertRefreshTokenAsync");
                return false;
            }
            finally
            {
                await t.DisposeAsync();
            }
        }
        /// <summary>
        /// 插入jwtToken相关数据到Redis中
        /// </summary>
        public async Task InsertToken2RedisAsync(UserRedis userRedis,string useKeyId)
        {
            var userRedisStr = userRedis.Serialize();
            // 使用 SetSlidingExpiration 将刷新token使用时自动更新时间，使其不过期
            var options = new DistributedCacheEntryOptions()
                            .SetSlidingExpiration(TimeSpan.FromSeconds(JwtTokenSettingConfig.JwtTokenSecretRefreshExpirationSeconds));
            await RedisCache.SetAsync("User_" + useKeyId, userRedisStr, options);
        }

        public async Task<string> GetUserIdToken4RedisAsync(string useKeyId)
        {
            return await RedisCache.GetStringAsync($"User_{useKeyId}");
        }
        #endregion
    }
}
