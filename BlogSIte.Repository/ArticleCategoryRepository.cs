﻿using BlogSite.CommonLib.CommonEntity;
using BlogSite.CommonLib.Extension;
using BlogSite.Database.Mysql;
using BlogSite.Entity.ArticleCategory;
using BlogSite.Repository.BaseRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSite.Repository
{
    public class ArticleCategoryRepository : BlogSiteRepository
    {
        #region Constructor
        public ArticleCategoryRepository(ILogger logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache)
            : base(logger, blogSiteDbContext, distributedCache)
        {
        }
        #endregion

        #region Public Method
        public async Task<bool> ValidatePostDataIsExistAsync(string articleCategoryName)
        {
            var result = await (from p in DbContext.ArticleCategory
                                where p.Name == articleCategoryName.Trim()
                                select p).FirstOrDefaultAsync();
            return result != null;
        }

        public async Task<bool> InsertData2MysqlAsync(ArticleCategoryCreatePostInput articleCategoryPostInput)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleCategory = new ArticleCategory
                {
                    Name = articleCategoryPostInput.Name,
                    UrlParam = articleCategoryPostInput.UrlParam,
                    Remark = articleCategoryPostInput.Remark,
                    KeyId = Guid.NewGuid().ToString(),
                    CreatedTime = DateTime.Now,
                };
                await DbContext.ArticleCategory.AddAsync(articleCategory);
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                Logger.LogError(e, "ArticleCategoryRepository Insert ArticleCategory Create Post");
                return false;
            }
            finally
            {
                await t.DisposeAsync();
            }
        }

        public async Task<bool> Delete4IdAsync(int categoryId)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleTag = await (from p in DbContext.ArticleCategory
                                        where p.Id == categoryId
                                        select p).FirstOrDefaultAsync();

                DbContext.ArticleCategory.Remove(articleTag);
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                throw new Exception($"ArticleCategoryRepository Delete ArticleTag {categoryId}", e);
            }
            finally
            {
                await t.DisposeAsync();
            }
        }
        public async Task<bool> EditData2MysqlAsync(ArticleCategoryEditPutInput articleCategoryEditPutInput)
        {
            using var t = await DbContext.Database.BeginTransactionAsync();
            try
            {
                var articleTag = await (from p in DbContext.ArticleCategory
                                        where p.Id == articleCategoryEditPutInput.Id
                                        select p).FirstOrDefaultAsync();
                articleTag.Name = articleCategoryEditPutInput.Name;
                articleTag.Remark = articleCategoryEditPutInput.Remark;
                await DbContext.SaveChangesAsync();
                await t.CommitAsync();
                return true;
            }
            catch (Exception e)
            {
                await t.RollbackAsync();
                Logger.LogError(e, "ArticleCategoryRepository Update ArticleCategory Edit Put");
                return false;
            }
            finally
            {
                await t.DisposeAsync();
            }
        }

        public async Task<ArticleCategory> FindFirstArticle4IdAsync(int categoryId)
        {
            return await (from p in DbContext.ArticleCategory
                          where p.Id == categoryId
                          select p).FirstOrDefaultAsync();
        }
        public async Task<ResultListEntity<ArticleCategory>> GetListAsync(CoditionEntity coditionEntity)
        {
            var query = (from p in DbContext.ArticleCategory
                         select p).AsQueryable();
            var findResult = query.UseCoditionFind(coditionEntity);
            var count = await findResult.Item1.CountAsync();
            coditionEntity.PaginationKeyValue.Count = count;
            var dataList = await findResult.Item2.ToListAsync();
            var result = new ResultListEntity<ArticleCategory>()
            {
                CoditionEntity = coditionEntity,
                Data = dataList
            };
            return result;
        }
        #endregion
    }
}
