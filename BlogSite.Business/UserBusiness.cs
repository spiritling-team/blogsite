﻿using BlogSite.Business.BaseBusiness;
using BlogSite.CommonLib;
using BlogSite.CommonLib.Extension;
using BlogSite.Database.Mysql;
using BlogSite.Database.Redis;
using BlogSite.Entity.Configs;
using BlogSite.Entity.User;
using BlogSite.Repository;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BlogSite.Business
{
    public class UserBusiness : BlogSiteJwtBusiness
    {

        #region Property
        public UserRepository Repo { get; private set; }
        #endregion

        #region Constructor
        public UserBusiness(ILogger logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtTokenSettingConfig, JwtSecurityTokenHandler tokenHandler)
            : base(logger, blogSiteDbContext, distributedCache, jwtTokenSettingConfig, tokenHandler)
        {
            Repo = new UserRepository(logger, blogSiteDbContext, distributedCache, jwtTokenSettingConfig, tokenHandler);
        }
        #endregion

        #region Public Method

        /// <summary>
        /// 验证用户密码和账号是否正确
        /// </summary>
        /// <param name="loginInput"></param>
        /// <returns></returns>
        public async Task<User> ValidateUserAsync(LoginInput loginInput)
        {
            var salt = await Repo.FindUserSalt4NameAsync(loginInput.UserName);
            if (salt.IsNullOrWhiteSpace())
                return null;
            loginInput.PassWord = loginInput.PassWord.EncryptUser(salt);
            var user = await Repo.FindUserInfo4LoginAsync(loginInput);
            return user;
        }

        /// <summary>
        /// 对用户发放JwtToken
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public async Task<JwtTokenOutput> CreateJwtTokenAsync(string keyId, string oldRefershToken = null, bool IsGenerate = true)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtTokenSettingConfig.JwtTokenSecret));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var claims = new ClaimsIdentity(new[] { new Claim("id", keyId.ToString()) });
            var security = new SecurityTokenDescriptor()
            {
                Issuer = JwtTokenSettingConfig.JwtTokenIssuer,
                Expires = DateTime.Now.AddSeconds(JwtTokenSettingConfig.JwtTokenAccessExpirationSeconds),
                Audience = JwtTokenSettingConfig.JwtTokenAudience,
                SigningCredentials = credentials,
                Subject = claims,
            };
            var jwtToken = TokenHandler.CreateJwtSecurityToken(security);
            var token = TokenHandler.WriteToken(jwtToken);
            return new JwtTokenOutput
            {
                UserKeyId = keyId,
                JwtToken = token,
                RefreshToken = IsGenerate ? Guid.NewGuid().ToString() : oldRefershToken
            };
        }

        /// <summary>
        /// 将刷新token存放到数据库中
        /// </summary>
        /// <param name="jwtTokenOutput"></param>
        /// <returns></returns>
        public async Task<bool> InsertRefreshTokenAsync(JwtTokenOutput jwtTokenOutput)
        {
            return await Repo.InsertRefreshTokenAsync(jwtTokenOutput);
        }

        /// <summary>
        /// 将刷新token插入到redis中
        /// </summary>
        public async Task InsertToken2RedisAsync(JwtTokenOutput jwtTokenOutput)
        {
            var userRedis = new UserRedis()
            {
                UserKeyId = jwtTokenOutput.UserKeyId,
                RefreshToken = jwtTokenOutput.RefreshToken
            };
            await Repo.InsertToken2RedisAsync(userRedis, jwtTokenOutput.UserKeyId.ToString());
        }

        /// <summary>
        /// 通过KeyId验证用户是否存在
        /// </summary>
        public async Task<UserRedis> GetUserRedis4KeyIdAsync(string keyId)
        {
            var userRedis = await Repo.GetUserIdToken4RedisAsync(keyId);
            return userRedis.IsNullOrWhiteSpace() ? null : userRedis.DeserializeRedisJson<UserRedis>();
        }

        #endregion
    }
}
