﻿using BlogSite.Database.Mysql;
using BlogSite.Entity.ArticleTag;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using BlogSite.Business.BaseBusiness;
using BlogSite.Repository;
using BlogSite.CommonLib.CommonEntity;

namespace BlogSite.Business
{
    public class ArticleTagBusiness:BlogSiteBusiness
    {
        #region Property
        public ArticleTagRepository Repo { get; set; }
        #endregion

        #region Constructor
        public ArticleTagBusiness(ILogger logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {
            Repo = new ArticleTagRepository(logger, mysqlDbContext, distributedCache);
        }
        #endregion

        #region Public Method

        public async Task<bool> ValidatePostDataIsExistAsync(string name)
        {
            return await Repo.ValidatePostDataIsExistAsync(name);
        }

        public async Task<bool> CreateArticelTagPostAsync(ArticleTagCreatePostInput articleTagPostInput)
        {
            return await Repo.InsertData2MysqlAsync(articleTagPostInput);
        }

        public async Task<bool> Delete4IdAsync(int tagId)
        {
            return await Repo.Delete4IdAsync(tagId);
        }

        public async Task<bool> EditArticelTagPutAsync(ArticleTagEditPutInput articleTagEditPutInput)
        {
            return await Repo.EditData2MysqlAsync(articleTagEditPutInput);
        }

        public async Task<ArticleTag> FindArticle4IdAsync(int tagId)
        {
            return await Repo.FindFirstArticle4IdAsync(tagId);
        }

        public async Task<ResultListEntity<ArticleTag>> GetListAsync(CoditionEntity coditionEntity)
        {
            return await Repo.GetListAsync(coditionEntity);
        }
        #endregion
    }

}
