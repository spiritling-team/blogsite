﻿using BlogSite.Database.Mysql;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using BlogSite.Business.BaseBusiness;
using BlogSite.Repository;
using BlogSite.CommonLib.CommonEntity;
using BlogSite.Entity.ArticleCategory;

namespace BlogSite.Business
{
    public class ArticleCategoryBussiness : BlogSiteBusiness
    {
        #region Property
        public ArticleCategoryRepository Repo { get; set; }
        #endregion

        #region Constructor
        public ArticleCategoryBussiness(ILogger logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {
            Repo = new ArticleCategoryRepository(logger, mysqlDbContext, distributedCache);
        }
        #endregion

        #region Public Method

        public async Task<bool> ValidatePostDataIsExistAsync(string name)
        {
            return await Repo.ValidatePostDataIsExistAsync(name);
        }

        public async Task<bool> CreateArticelCategorygPostAsync(ArticleCategoryCreatePostInput articleCategoryPostInput)
        {
            return await Repo.InsertData2MysqlAsync(articleCategoryPostInput);
        }

        public async Task<bool> Delete4IdAsync(int categoryId)
        {
            return await Repo.Delete4IdAsync(categoryId);
        }

        public async Task<bool> EditArticelCategoryPutAsync(ArticleCategoryEditPutInput articleCategoryEditPutInput)
        {
            return await Repo.EditData2MysqlAsync(articleCategoryEditPutInput);
        }

        public async Task<ArticleCategory> FindArticle4IdAsync(int categoryId)
        {
            return await Repo.FindFirstArticle4IdAsync(categoryId);
        }

        public async Task<ResultListEntity<ArticleCategory>> GetListAsync(CoditionEntity coditionEntity)
        {
            return await Repo.GetListAsync(coditionEntity);
        }
        #endregion
    }

}
