﻿using BlogSite.Database.Mysql;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;

namespace BlogSite.Business.BaseBusiness
{
    public class BlogSiteBusiness : IDisposable
    {
        public ILogger Logger { get; protected set; }
        public BlogSiteDbContext DbContext { get; protected set; }
        public IDistributedCache RedisCache { get; protected set; }

        public BlogSiteBusiness(ILogger logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
        {
            Logger = logger;
            DbContext = mysqlDbContext;
            RedisCache = distributedCache;
        }
        public void Dispose()
        {
            // Method intentionally left empty.
        }
    }
}
