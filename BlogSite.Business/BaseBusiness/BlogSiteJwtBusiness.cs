﻿using BlogSite.Database.Mysql;
using BlogSite.Entity.Configs;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;

namespace BlogSite.Business.BaseBusiness
{
    public class BlogSiteJwtBusiness : BlogSiteBusiness
    {
        public JwtSecurityTokenHandler TokenHandler { get; protected set; }

        public JwtTokenSettingConfig JwtTokenSettingConfig { get; protected set; }

        public BlogSiteJwtBusiness(ILogger logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtToeknSettingConfig, JwtSecurityTokenHandler tokenHandler)
            : base(logger, mysqlDbContext, distributedCache)
        {
            TokenHandler = tokenHandler;
            JwtTokenSettingConfig = jwtToeknSettingConfig.Value;
        }
    }
}
