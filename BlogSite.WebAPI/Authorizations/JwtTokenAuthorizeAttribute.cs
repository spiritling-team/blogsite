﻿using BlogSite.Entity.Configs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;

namespace BlogSite.WebAPI.Authorizations
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
    public class JwtTokenAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public JwtTokenAuthorizeAttribute()
        {

        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var services = context.HttpContext.RequestServices;

                var jwtToeknSettingConfig = services.GetService<IOptions<JwtTokenSettingConfig>>().Value;
                var tokenHandler = services.GetService<JwtSecurityTokenHandler>();

                var token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

                var claims = tokenHandler.ValidateToken(token, new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtToeknSettingConfig.JwtTokenSecret)),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidIssuer = jwtToeknSettingConfig.JwtTokenIssuer,
                    ValidAudience = jwtToeknSettingConfig.JwtTokenAudience,
                    // 时钟偏移，默认五分钟。在过期后五分钟内依旧有效
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken securityToken);
                var claimsList = claims.Claims.ToList();
                var userId = string.Empty;
                for (int i = 0; i < claimsList.Count; i++)
                {
                    if (claimsList[i].Type == "id")
                    {
                        userId = claimsList[i].Value;
                    }
                }
                context.HttpContext.Request.Headers["UserId"] = userId;
            }
            catch
            {
                context.Result = new StatusCodeResult(701);
                return;
            }
        }
    }
}
