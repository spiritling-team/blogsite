﻿using AutoMapper;
using BlogSite.Database.Mysql;
using BlogSite.Entity.Configs;
using BlogSite.Entity.ResultDTO;
using BlogSite.WebAPI.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text.Json;

namespace BlogSite.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            // 添加 JwtSecurityTokenHandler 实例化
            services.AddScoped<JwtSecurityTokenHandler, JwtSecurityTokenHandler>();

            // 添加 Swagger UI
            services.AddSwaggerGen();

            // 添加AutoMapper
            services.AddAutoMapper(typeof(Startup));

            // 添加自定义Options
            services.Configure<JwtTokenSettingConfig>(Configuration.GetSection("JwtToeknSettingConfig"));

            // mysql 数据库链接
            services.AddDbContext<BlogSiteDbContext>(options => options.UseMySql(Configuration.GetConnectionString("MySQL")));

            // redis 数据库连接
            services.AddStackExchangeRedisCache(options =>
            {
                // 自定义配置
                var config = new ConfigurationOptions()
                {
                    EndPoints = { { Configuration.GetConnectionString("Redis") } }
                };
                options.ConfigurationOptions = config;
            });

            // 序列化输出的数据对象
            services.AddControllers().AddXmlDataContractSerializerFormatters().AddJsonOptions(options =>
            {
                // 将首字母大写改为驼峰命名
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                // Configure a custom converter
                // options.JsonSerializerOptions.Converters.Add(new MyCustomJsonConverter());
            });

            // 使用MVC的数据验证，自定义返回结果合计
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = (context) =>
                {
                    var errorList = context.ModelState
                            .Where(x => x.Value.Errors.Count > 0)
                            .ToDictionary(
                                kvp => kvp.Key,
                                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToList()
                            );

                    var dataError = ResultDTO<Dictionary<string, List<string>>>.DataValidationErrorResult(errorList);
                    // 这边进行随便某一个结果返回，指定code为801，表示请求参数校验失败，在中间件中进行处理
                    var result = new BadRequestObjectResult(dataError)
                    {
                        StatusCode = 801
                    };
                    return result;
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {

            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });
                app.UseDeveloperExceptionPage();
            }
            // 跨域设置,生产环境使用特性进行处理
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            loggerFactory.AddLog4Net();

            app.UseResponseCodeChange4Body();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
