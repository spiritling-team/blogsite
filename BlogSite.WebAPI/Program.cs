﻿using BlogSite.CommonLib;
using BlogSite.Database.Mysql;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace BlogSite.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            CreateDbIfNotExists(host);

            host.Run();
        }

        private static void CreateDbIfNotExists(IHost host)
        {
            using var scope = host.Services.CreateScope();

            var services = scope.ServiceProvider;
            var env = services.GetRequiredService<IWebHostEnvironment>();
            var logger = services.GetRequiredService<ILogger<Program>>();
            try
            {
                if (env.IsDevelopment())
                {
                    //var context = services.GetRequiredService<BlogSiteDbContext>();

                    //try
                    //{
                    //    logger.LogInformation("-----Database Deleted------");
                    //    context.Database.EnsureDeleted();
                    //    logger.LogInformation("-----Database Created------");
                    //    context.Database.EnsureCreated();
                    //    //创建默认账号
                    //    var guid = Guid.NewGuid();
                    //    var password = $"spiritxA123456ling{guid}";
                    //    var user = new User()
                    //    {
                    //        UserName = "xp",
                    //        Salt = guid.ToString(),
                    //        KeyId = Guid.Parse("4A9783AD-2B04-4866-893D-8232C5ED88C0").ToString(),
                    //        PassWord = password.ConvertSHA512(),
                    //        CreatedTime = DateTime.Now,
                    //        UpdatedTime = DateTime.Now
                    //    };
                    //    context.User.Add(user);

                    //    context.SaveChanges();

                    //}
                    //catch (Exception ex)
                    //{
                    //    logger.LogError(ex, "An error occurred creating the DB.");
                    //}
                }
                else
                {
                    // 正式发布后，启动程序后，如果mysql数据库中存在refreshtken时，并且redis中不存在时，则将其放置到redis中去。
                    var redisCache = services.GetRequiredService<IDistributedCache>();
                }
            }
            catch (Exception e)
            {
                logger.LogError(e, "Program Start Error");
            }

        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
        }

    }
}
