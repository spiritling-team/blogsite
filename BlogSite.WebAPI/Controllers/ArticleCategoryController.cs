﻿using BlogSite.Business;
using BlogSite.CommonLib.CommonEntity;
using BlogSite.Database.Mysql;
using BlogSite.Entity.ArticleCategory;
using BlogSite.Entity.ResultDTO;
using BlogSite.WebAPI.Authorizations;
using BlogSite.WebAPI.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.Json;

namespace BlogSite.WebAPI.Controllers
{
    [JwtTokenAuthorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ArticleCategoryController : BlogSiteController<ArticleCategoryController>
    {
        #region Property
        public ArticleCategoryBussiness Biz { get; set; }
        #endregion

        #region Constructor
        public ArticleCategoryController(ILogger<ArticleCategoryController> logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {
            Biz = new ArticleCategoryBussiness(logger, mysqlDbContext, distributedCache);
        }
        #endregion



        /// <summary>
        /// 创建新的数据
        /// </summary>
        [HttpPost]
        public async Task<IResultDTO<string>> PostAsync([FromBody] ArticleCategoryCreatePostInput articleCategoryPostInput)
        {
            try
            {
                articleCategoryPostInput.Name = articleCategoryPostInput.Name.Trim();
                var isInsert = await Biz.CreateArticelCategorygPostAsync(articleCategoryPostInput);
                if (isInsert)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleCategoryController PostAsync");
                return ResultDTO<string>.InternalServerErrorResult();
            }

        }

        /// <summary>
        /// 删除
        /// </summary>
        [HttpDelete("{categoryId}")]
        public async Task<IResultDTO<string>> Delete4IdAsync([FromRoute] int categoryId)
        {
            try
            {
                Logger.LogError($"ArticleCategory Delete {categoryId} {DateTime.Now}");
                var bol = await Biz.Delete4IdAsync(categoryId);
                if (bol)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleCategoryController Delete");
                return ResultDTO<string>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 修改数据
        /// </summary>
        [HttpPut]
        public async Task<IResultDTO<string>> Put4IdAsync([FromBody] ArticleCategoryEditPutInput articleCategoryEditPutInput)
        {
            try
            {
                articleCategoryEditPutInput.Name = articleCategoryEditPutInput.Name.Trim();
                // 判断id是否存在
                var article = await Biz.FindArticle4IdAsync(articleCategoryEditPutInput.Id);
                if (article == null)
                {
                    var errorList = new Dictionary<string, List<string>>
                    {
                        { "Id", new List<string>() { "Id is not exist in database." } }
                    };
                    return ResultDTO<string>.DataValidationErrorResult(errorList);
                }
                var isEdit = await Biz.EditArticelCategoryPutAsync(articleCategoryEditPutInput);
                if (isEdit)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleCategoryController PostAsync");
                return ResultDTO<string>.InternalServerErrorResult();
            }

        }

        /// <summary>
        /// 获取指定id数据
        /// </summary>
        [HttpGet("{categoryId}")]
        public async Task<IResultDTO<ArticleCategory>> GetArticle4IdAsync([FromRoute] int categoryId)
        {
            try
            {
                var result = await Biz.FindArticle4IdAsync(categoryId);
                return new ResultDTO<ArticleCategory>(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleCategoryController GetArticle4IdAsync");
                return ResultDTO<ArticleCategory>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 获取所有数据
        /// </summary>
        [HttpGet]
        public async Task<IResultDTO<ResultListEntity<ArticleCategory>>> GetListAsync([FromQuery] string codingtionStr)
        {
            try
            {
                CoditionEntity coditionEntity = JsonSerializer.Deserialize<CoditionEntity>(codingtionStr);
                var result = await Biz.GetListAsync(coditionEntity);
                return new ResultDTO<ResultListEntity<ArticleCategory>>(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleCategoryController GetListAsync");
                return ResultDTO<ResultListEntity<ArticleCategory>>.InternalServerErrorResult();
            }
        }
    }
}
