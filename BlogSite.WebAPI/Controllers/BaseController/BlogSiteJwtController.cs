﻿using Microsoft.Extensions.Logging;
using BlogSite.Database.Mysql;
using Microsoft.Extensions.Caching.Distributed;
using BlogSite.Entity.Configs;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;

namespace BlogSite.WebAPI.Controllers.BaseController
{
    public class BlogSiteJwtController<T>:BlogSiteController<T>
    {
        public JwtSecurityTokenHandler TokenHandler { get; protected set; }
        public JwtTokenSettingConfig JwtToeknSettingConfig { get; protected set; }
        public BlogSiteJwtController(ILogger<T> logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtToeknSettingConfig, JwtSecurityTokenHandler tokenHandler)
            :base(logger,mysqlDbContext,distributedCache)
        {
            TokenHandler = tokenHandler;
            JwtToeknSettingConfig = jwtToeknSettingConfig.Value;
        }
    }
}
