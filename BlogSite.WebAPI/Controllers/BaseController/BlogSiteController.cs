﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BlogSite.Database.Mysql;
using Microsoft.Extensions.Caching.Distributed;
using BlogSite.Entity.Configs;
using Microsoft.Extensions.Options;
using System;
using Microsoft.AspNetCore.Http;

namespace BlogSite.WebAPI.Controllers.BaseController
{
    public class BlogSiteController<T> : ControllerBase, IDisposable
    {
        public ILogger<T> Logger { get; protected set; }
        public BlogSiteDbContext DbContext { get; protected set; }
        public IDistributedCache RedisCache { get; protected set; }

        public BlogSiteController(ILogger<T> logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache)
        {
            Logger = logger;
            DbContext = blogSiteDbContext;
            RedisCache = distributedCache;
        }
        // https://stackoverflow.com/a/53632914
        [NonAction]
        public void Dispose()
        {
            // Method intentionally left empty.
        }
    }
}
