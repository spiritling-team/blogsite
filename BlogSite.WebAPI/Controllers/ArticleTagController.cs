﻿using BlogSite.Business;
using BlogSite.CommonLib.CommonEntity;
using BlogSite.Database.Mysql;
using BlogSite.Entity.ArticleTag;
using BlogSite.Entity.ResultDTO;
using BlogSite.WebAPI.Authorizations;
using BlogSite.WebAPI.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.Json;

namespace BlogSite.WebAPI.Controllers
{
    [JwtTokenAuthorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ArticleTagController : BlogSiteController<ArticleTagController>
    {
        #region Property
        public ArticleTagBusiness Biz { get; set; }
        #endregion

        #region Constructor
        public ArticleTagController(ILogger<ArticleTagController> logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {
            Biz = new ArticleTagBusiness(logger, mysqlDbContext, distributedCache);
        }
        #endregion

        

        /// <summary>
        /// 创建新的数据
        /// </summary>
        [HttpPost]
        public async Task<IResultDTO<string>> PostAsync([FromBody] ArticleTagCreatePostInput articleTagPostInput)
        {
            try
            {
                articleTagPostInput.Name = articleTagPostInput.Name.Trim();
                var isInsert = await Biz.CreateArticelTagPostAsync(articleTagPostInput);
                if (isInsert)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleTagController PostAsync");
                return ResultDTO<string>.InternalServerErrorResult();
            }

        }

        /// <summary>
        /// 删除
        /// </summary>
        [HttpDelete("{tagId}")]
        public async Task<IResultDTO<string>> Delete4IdAsync([FromRoute] int tagId)
        {
            try
            {
                Logger.LogError($"ArticleTag Delete {tagId} {DateTime.Now}");
                var bol = await Biz.Delete4IdAsync(tagId);
                if (bol)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleTagController Delete");
                return ResultDTO<string>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 修改数据
        /// </summary>
        [HttpPut]
        public async Task<IResultDTO<string>> Put4IdAsync([FromBody] ArticleTagEditPutInput articleTagEditPutInput)
        {
            try
            {
                articleTagEditPutInput.Name = articleTagEditPutInput.Name.Trim();
                // 判断id是否存在
                var article = await Biz.FindArticle4IdAsync(articleTagEditPutInput.Id);
                if(article == null)
                {
                    var errorList = new Dictionary<string, List<string>>
                    {
                        { "Id", new List<string>() { "Id is not exist in database." } }
                    };
                    return ResultDTO<string>.DataValidationErrorResult(errorList);
                }
                var isEdit = await Biz.EditArticelTagPutAsync(articleTagEditPutInput);
                if (isEdit)
                {
                    return ResultDTO<string>.OkResult();
                }
                else
                {
                    return ResultDTO<string>.InternalServerErrorResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleTagController PostAsync");
                return ResultDTO<string>.InternalServerErrorResult();
            }

        }

        /// <summary>
        /// 获取指定id数据
        /// </summary>
        [HttpGet("{tagId}")]
        public async Task<IResultDTO<ArticleTag>> GetArticle4IdAsync([FromRoute] int tagId)
        {
            try
            {
                var result = await Biz.FindArticle4IdAsync(tagId);
                return new ResultDTO<ArticleTag>(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleTagController GetArticle4IdAsync");
                return ResultDTO<ArticleTag>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 获取所有数据
        /// </summary>
        [HttpGet]
        public async Task<IResultDTO<ResultListEntity<ArticleTag>>> GetListAsync([FromQuery] string codingtionStr)
        {
            try
            {
                CoditionEntity coditionEntity = JsonSerializer.Deserialize<CoditionEntity>(codingtionStr);
                var result = await Biz.GetListAsync(coditionEntity);
                return new ResultDTO<ResultListEntity<ArticleTag>>(result);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleTagController GetListAsync");
                return ResultDTO<ResultListEntity<ArticleTag>>.InternalServerErrorResult();
            }
        }
    }
}
