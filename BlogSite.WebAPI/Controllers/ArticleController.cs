﻿using BlogSite.Database.Mysql;
using BlogSite.Entity.Article;
using BlogSite.Entity.ResultDTO;
using BlogSite.WebAPI.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BlogSite.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticleController : BlogSiteController<ArticleController>
    {
        public ArticleController(ILogger<ArticleController> logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {
        }

        [HttpPost]
        public async Task<IResultDTO<string>> PostAsync([FromBody] ArticleDTO articlePostInput)
        {
            try
            {
                return ResultDTO<string>.OkResult();
            }
            catch (Exception e)
            {
                Logger.LogError(e, "ArticleController PostAsync");
                return ResultDTO<string>.InternalServerErrorResult();
            }

        }
    }
}
