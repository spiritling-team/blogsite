﻿using BlogSite.Entity.ResultDTO;
using BlogSite.Database.Mysql;
using BlogSite.Entity.Configs;
using BlogSite.Business;
using BlogSite.Entity.User;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using BlogSite.WebAPI.Controllers.BaseController;
using BlogSite.WebAPI.Authorizations;

namespace BlogSite.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : BlogSiteJwtController<UserController>
    {
        public UserBusiness Biz { get; private set; }

        public UserController(ILogger<UserController> logger, BlogSiteDbContext blogSiteDbContext, IDistributedCache distributedCache, IOptions<JwtTokenSettingConfig> jwtToeknSettingConfig, JwtSecurityTokenHandler tokenHandler)
            : base(logger, blogSiteDbContext, distributedCache, jwtToeknSettingConfig, tokenHandler)
        {
            Biz = new UserBusiness(logger, blogSiteDbContext, distributedCache, jwtToeknSettingConfig, tokenHandler);
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="loginInput"></param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IResultDTO<JwtTokenOutput>> LoginAsync([FromBody] LoginInput loginInput)
        {
            try
            {
                // 验证参数必填项
                if(loginInput.UserName==null || loginInput.PassWord == null)
                {
                    return ResultDTO<JwtTokenOutput>.BadRequestResult();
                }
                // 验证用户
                var user = await Biz.ValidateUserAsync(loginInput);
                if (user == null)
                {
                    return ResultDTO<JwtTokenOutput>.BadRequestResult();
                }
                // 创建jwt
                var jwt = await Biz.CreateJwtTokenAsync(user.KeyId);
                // 插入mysql
                var insertStatus = await Biz.InsertRefreshTokenAsync(jwt);
                if (!insertStatus)
                    return ResultDTO<JwtTokenOutput>.InternalServerErrorResult();
                // 插入redis
                await Biz.InsertToken2RedisAsync(jwt);
                return new ResultDTO<JwtTokenOutput>(jwt);
            }
            catch (Exception e)
            {
                Logger.LogError(e, "User LoginAsync Error");
                return ResultDTO<JwtTokenOutput>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 刷新token值
        /// </summary>
        /// <returns></returns>
        [HttpPost("refreshToken")]
        public async Task<IResultDTO<JwtTokenOutput>> RefreshTokenAsync([FromBody] RefreshTokenInput refreshTokenInput)
        {
            try
            {
                // 验证参数必填项
                if (refreshTokenInput.RefreshToken.Equals(Guid.Empty) || refreshTokenInput.KeyId.Equals(Guid.Empty))
                {
                    return ResultDTO<JwtTokenOutput>.BadRequestResult();
                }
                // 验证用户是否存在和验证刷新token是否存在或者过期
                var userRedis = await Biz.GetUserRedis4KeyIdAsync(refreshTokenInput.KeyId);
                if (userRedis != null && userRedis.RefreshToken.Equals(refreshTokenInput.RefreshToken))
                {
                    var jwt = await Biz.CreateJwtTokenAsync(refreshTokenInput.KeyId, userRedis.RefreshToken, false);
                    return new ResultDTO<JwtTokenOutput>(jwt);
                }
                else
                {
                    return ResultDTO<JwtTokenOutput>.UnauthorizedResult();
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e, "User RefreshTokenAsync Error");
                return ResultDTO<JwtTokenOutput>.InternalServerErrorResult();
            }
        }

        /// <summary>
        /// 验证token值，其实就是一个空业务，只不过加上了权限验证，过一遍权限验证，看看是否登录着
        /// </summary>
        /// <returns></returns>
        [JwtTokenAuthorize]
        [HttpPost("VerifyToken")]
        public async Task<IResultDTO<string>> VerifyTokenAsync()
        {
            try
            {
                return ResultDTO<string>.OkResult();
            }
            catch (Exception e)
            {
                Logger.LogError(e, "User VerifyTokenAsync Error");
                return ResultDTO<string>.InternalServerErrorResult();
            }
        }
    }
}
