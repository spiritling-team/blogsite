﻿using BlogSite.Entity.ResultDTO;
using BlogSite.Database.Mysql;
using BlogSite.WebAPI.Controllers.BaseController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using BlogSite.WebAPI.Authorizations;

namespace BlogSite.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : BlogSiteController<WeatherForecastController>
    {

        public WeatherForecastController(ILogger<WeatherForecastController> logger, BlogSiteDbContext mysqlDbContext, IDistributedCache distributedCache)
            : base(logger, mysqlDbContext, distributedCache)
        {

        }
        [HttpGet]
        [JwtTokenAuthorize]
        public async Task<IResultDTO<string>> GetAsync()
        {
            return ResultDTO<string>.OkResult();
        }
    }
}
