﻿using BlogSite.Entity.ResultDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BlogSite.WebAPI.Middlewares
{
    /// <summary>
    /// 修改返回的Response的状态码，标准返回格式为 IResultDTO<T>
    /// 将 IResultDTO 中的 code 赋值给返回状态码
    /// </summary>
    public class ResponseCodeChange4BodyMiddleware
    {
        private readonly RequestDelegate _next;
        public readonly ILogger<ResponseCodeChange4BodyMiddleware> _logger;

        public ResponseCodeChange4BodyMiddleware(RequestDelegate next,ILogger<ResponseCodeChange4BodyMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            //using (var reader = new StreamReader(resquest.Body, Encoding.UTF8))
            //{
            // 必须保留研究
            //    reader.BaseStream.Seek(0, SeekOrigin.Begin);
            //    var readerStr = await reader.ReadToEndAsync();
            //    reader.BaseStream.Seek(0, SeekOrigin.Begin);
            //}

            var originalResponseStream = httpContext.Response.Body;

            // https://www.cnblogs.com/lwqlun/p/10954936.html
            // 这里使用可读写的MemoryStream来替换body数据
            using (var ms = new MemoryStream())
            {
                var path = httpContext.Request.Path.Value;
                var pathRegex = @"^\/api\/";

                // 替换时一定要放到 _next 之前，否则会将其他的地方返回的response给替换掉
                httpContext.Response.Body = ms;
                await _next(httpContext);

                // 701 状态码时，代表jwttoken过期，单独处理
                if (httpContext.Response.StatusCode == 701)
                {
                    var body = new ResultDTO<string>()
                    {
                        Code = 701,
                        MsgKey = "Jwt Token Expired",
                        Message = "Jwt token expired, permission verification failed."
                    };
                    var bodyByte = JsonSerializer.SerializeToUtf8Bytes(body);

                    httpContext.Response.Body.Write(bodyByte, 0, bodyByte.Length);
                }
                // 请求/api/下的接口时，按照规范格式化
                else if(Regex.IsMatch(path, pathRegex, RegexOptions.IgnoreCase))
                {
                    try
                    {
                        ms.Position = 0;
                        var responseReader = new StreamReader(ms);

                        var responseContent = await responseReader.ReadToEndAsync();

                        var resultPrototype = JsonSerializer.Deserialize<ResultPrototype>(responseContent);

                        // 状态码为 801 的一律放过去
                        if(httpContext.Response.StatusCode != 801)
                        {
                            // 过滤那些可以反序列化，但是接口类型匹配不上的
                            if (resultPrototype.Code == 0 && !resultPrototype.Success && resultPrototype.Message == null && resultPrototype.MsgKey == null)
                            {
                                _logger.LogWarning($"Response Body Context type mismatch ResultPrototype \n Response Content: {responseContent}");
                                // _logger.LogError($"Response Body Context type mismatch ResultPrototype \n Response Content: {responseContent}");
                            }
                            else
                            {
                                httpContext.Response.StatusCode = (int)resultPrototype.Code;
                            }
                        }

                        ms.Position = 0;

                        await ms.CopyToAsync(originalResponseStream);
                        httpContext.Response.Body = originalResponseStream;

                    }
                    catch(Exception e)
                    {
                        _logger.LogError(e, "ResponseCodeChange4Body Error");

                        ms.Position = 0;

                        await ms.CopyToAsync(originalResponseStream);
                        httpContext.Response.Body = originalResponseStream;
                    }
                }
            }

        }
    }
}
