﻿using Microsoft.AspNetCore.Builder;

namespace BlogSite.WebAPI.Middlewares
{
    public static class MiddleWareExtensions
    {
        public static IApplicationBuilder UseResponseCodeChange4Body(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseCodeChange4BodyMiddleware>();
        }
    }
}
