import React from "react";

// 类型枚举
export enum ReduxType {
    Loading = 0 << 1,
}

// Action 接口
export interface IAction<T> {
    type: ReduxType;
    payload: T;
}

// 拆分IReduxContext

interface IReduxLoading {
    IsLoading: boolean;
}

export interface IReduxContext extends IReduxLoading {}

// 默认值
export let defaultContext: IReduxContext = {
    IsLoading: false,
};

export const ReduxContext = React.createContext<IReduxContext>(defaultContext);

let loadingReducer = function (
    state: IReduxContext = defaultContext,
    action: IAction<IReduxLoading>
) {
    switch (action.type) {
        case ReduxType.Loading:
            return { ...state, ...action.payload };
        default:
            return { ...state };
    }
};

export const ReducerList = {
    loadingReducer,
};
