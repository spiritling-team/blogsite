/**
 * @description 全局扩展接口
 */
interface Window {
    /**
     * @description react 路由基础路径
     */
    _REACT_BASE_URL: string;
    /**
     * @description 阿里巴巴矢量库地址
     */
    _ICON_FONT_URL: string;
}

/**
 * @description 全局DTO
 */
declare class GlobalResultDto<T = any> {
    /**
     * @description 返回状态码
     */
    code: number;
    /**
     * @description 返回的数据
     */
    data?: T;
    /**
     * @description 返回消息
     */
    message: string;
    /**
     * @description 消息Key
     */
    msgKey: string;
    /**
     * @description 是否成功
     */
    success: boolean;
    /**
     * @description 错误列表，只有在状态码为801时
     */
    errorList: {
        [key: string]: string[];
    };
}

/**
 * @description 局部Redux使用时的通用Action
 * @interface IPartialAction
 * @template T Type类型，用于Redux中的switch判断
 * @template P payload 更新的数据类型
 */
interface IPartialAction<T, P> {
    type: T;
    payload: P;
}

/**
 * @description ReactCreateContext全局通用类型
 * @interface StateContext
 * @template T Type类型，用于Redux中的switch判断
 * @template P payload 更新的数据类型
 * @example React.createContext({} as StateContext<P, T>);
 */
interface StateContext<P, T> {
    state: T;
    setState: React.Dispatch<IPartialAction<P, T>>;
}

interface PaginationKeyValue {
    enable: boolean;
    count: number;
    pageSize: number;
    pageNumber: number;
}

interface OrderByKeyValue {
    enable: boolean;
    propertyName: string;
    orderDirection: 0 | 1;
}

interface SearchKeyValue {
    enable: boolean;
    propertyName: string;
    searchContent: string;
}

interface CoditionEntity {
    paginationKeyValue: PaginationKeyValue;
    orderByKeyValue: OrderByKeyValue;
    searchKeyValue: SearchKeyValue;
}
