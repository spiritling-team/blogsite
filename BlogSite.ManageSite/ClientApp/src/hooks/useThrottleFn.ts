import { throttle } from "lodash";
import { useRef } from "react";
import useCreation from "./useCreation";
import { IThrottleOptions } from "interface/hooks";

type Fn = (...args: any) => any;

/**
 * @description 用来处理节流函数的 Hook。
 * @link https://ahooks.js.org/zh-CN/hooks/side-effect/use-throttle-fn
 */
function useThrottleFn<T extends Fn>(fn: T, options?: IThrottleOptions) {
    const fnRef = useRef<T>(fn);
    fnRef.current = fn;

    const wait = options?.wait ?? 1000;

    const throttled = useCreation(
        () =>
            throttle<T>(
                ((...args: any[]) => {
                    return fnRef.current(...args);
                }) as T,
                wait,
                options
            ),
        []
    );

    return {
        run: (throttled as unknown) as T,
        cancel: throttled.cancel,
        flush: throttled.flush,
    };
}

export default useThrottleFn;
