import { useMemo } from "react";
import useToggle from "./useToggle";

export interface IBooleanActions {
    setTrue: () => void;
    setFalse: () => void;
    toggle: (value?: boolean | undefined) => void;
}

/**
 * @description 优雅的管理 boolean 值的 Hook。
 * @link https://ahooks.js.org/zh-CN/hooks/state/use-boolean
 */
export default function useBoolean(
    defaultValue = false
): [boolean, IBooleanActions] {
    const [state, { toggle }] = useToggle(defaultValue);

    const actions: IBooleanActions = useMemo(() => {
        const setTrue = () => toggle(true);
        const setFalse = () => toggle(false);
        return { toggle, setTrue, setFalse };
    }, [toggle]);

    return [state, actions];
}
