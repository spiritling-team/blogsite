import { useState, useEffect } from "react";
import useDebounceFn from "./useDebounceFn";
import { IDebounceOptions } from "interface/hooks";

/**
 * @description 用来处理防抖值的 Hook。
 * @link https://ahooks.js.org/zh-CN/hooks/side-effect/use-debounce
 */
export default function useDebounce<T>(value: T, options?: IDebounceOptions) {
    const [debounced, setDebounced] = useState(value);

    const { run } = useDebounceFn(() => {
        setDebounced(value);
    }, options);

    useEffect(() => {
        run();
    }, [value]);

    return debounced;
}
