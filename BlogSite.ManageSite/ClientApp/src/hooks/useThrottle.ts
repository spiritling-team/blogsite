import { useState, useEffect } from "react";
import useThrottleFn from "./useThrottleFn";
import { IThrottleOptions } from "interface/hooks";

/**
 * @description 用来处理节流值的 Hook。
 * @link https://ahooks.js.org/zh-CN/hooks/side-effect/use-throttle
 */
function useThrottle<T>(value: T, options?: IThrottleOptions) {
    const [throttled, setThrottled] = useState(value);

    const { run } = useThrottleFn(() => {
        setThrottled(value);
    }, options);

    useEffect(() => {
        run();
    }, [value]);

    return throttled;
}

export default useThrottle;
