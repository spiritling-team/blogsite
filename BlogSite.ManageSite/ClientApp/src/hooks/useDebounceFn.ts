import { debounce } from "lodash";
import { useRef } from "react";
import useCreation from "./useCreation";
import { IDebounceOptions } from "interface/hooks";

type Fn = (...args: any) => any;

/**
 * @description 用来处理防抖函数的 Hook。
 * @link https://ahooks.js.org/zh-CN/hooks/side-effect/use-debounce-fn
 */
function useDebounceFn<T extends Fn>(fn: T, options?: IDebounceOptions) {
    const fnRef = useRef<T>(fn);
    fnRef.current = fn;

    const wait = options?.wait ?? 1000;

    const debounced = useCreation(
        () =>
            debounce<T>(
                ((...args: any[]) => {
                    return fnRef.current(...args);
                }) as T,
                wait,
                options
            ),
        []
    );

    return {
        run: (debounced as unknown) as T,
        cancel: debounced.cancel,
        flush: debounced.flush,
    };
}

export default useDebounceFn;
