import { Location } from "history";
import React, { useEffect, useState } from "react";
import { Prompt } from "react-router-dom";
import { Modal } from "antd";

interface Props {
    when?: boolean | undefined;
    navigate: (path: string) => void;
    shouldBlockNavigation: (location: Location) => boolean;
    modal?: ModalInfo;
}

interface ModalInfo {
    title?: string;
    content?: string;
    okText?: string;
    cancelText?: string;
}

const RouteLeavingGuard = ({
    when,
    navigate,
    shouldBlockNavigation,
    modal,
}: Props) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [lastLocation, setLastLocation] = useState<Location | null>(null);
    const [confirmedNavigation, setConfirmedNavigation] = useState(false);
    const closeModal = () => {
        setModalVisible(false);
    };
    const handleBlockedNavigation = (nextLocation: Location): boolean => {
        if (typeof shouldBlockNavigation === "function") {
            if (!confirmedNavigation && shouldBlockNavigation(nextLocation)) {
                setModalVisible(true);
                setLastLocation(nextLocation);
                return false;
            }
        }

        return true;
    };
    const handleConfirmNavigationClick = () => {
        setModalVisible(false);
        setConfirmedNavigation(true);
    };
    useEffect(() => {
        if (confirmedNavigation && lastLocation) {
            // Navigate to the previous blocked location with your navigate function
            navigate(lastLocation.pathname);
        }
    }, [confirmedNavigation, lastLocation]);
    return (
        <>
            <Prompt when={when} message={handleBlockedNavigation} />
            {/* Your own alert/dialog/modal component */}
            <Modal
                title={modal ? modal.title ?? "Tips" : "Tips"}
                visible={modalVisible}
                onOk={handleConfirmNavigationClick}
                onCancel={closeModal}
                okText={modal ? modal.okText ?? "OK" : "OK"}
                cancelText={modal ? modal.okText ?? "Cancel" : "Cancel"}>
                <p>{modal ? modal.content ?? "测试" : "测试"}</p>
            </Modal>
        </>
    );
};

export default RouteLeavingGuard;
