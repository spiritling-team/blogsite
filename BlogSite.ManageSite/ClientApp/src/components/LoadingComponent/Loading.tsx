import React, { useEffect, useState } from "react";
import { Spin } from "antd";
import styles from "./Loading.module.scss";

export default () => {
    const [status, setStatus] = useState<boolean>(true);
    useEffect(
        function () {
            setStatus(true);
            return function () {
                setStatus(false);
            };
        },
        [status]
    );
    return (
        <Spin spinning={status} tip='Loading...'>
            <div className={styles.loadingMainDiv}></div>
        </Spin>
    );
};
