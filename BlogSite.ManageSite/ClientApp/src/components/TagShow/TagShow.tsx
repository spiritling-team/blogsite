import React from "react";
import styles from "./TagShow.module.scss";

export interface Props {
    bgColor?: string;
    textColor?: string;
    text: string;
}

export default (props: Props) => {
    return (
        <span
            className={styles.tagShowWarp}
            style={{
                color: props.textColor ?? "#FFF",
                backgroundColor: props.bgColor ?? "#000",
            }}>
            {props.text}
        </span>
    );
};
