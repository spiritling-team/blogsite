export interface IMenuItem {
    key: string;
    path?: string;
    title: string;
    icon?: string;
    children: IMenuItem[];
}

/**
 * key 最好和path一致，用于路由跳转方便
 */
export const MenuItem: IMenuItem[] = [
    {
        key: "/dashboard",
        path: "/dashboard",
        title: "Dashboard",
        icon: "bs_dashboard",
        children: [],
    },
    {
        key: "article",
        title: "文章相关",
        icon: "bs_wenzhang",
        children: [
            {
                key: "/articleList",
                path: "/articleList",
                title: "文章列表",
                icon: "bs_liebiao",
                children: [],
            },
            {
                key: "/articleCategory",
                path: "/articleCategory",
                title: "文章分类",
                icon: "bs_icon",
                children: [],
            },
            {
                key: "/articleTag",
                path: "/articleTag",
                title: "文章标签",
                icon: "bs_biaoqian",
                children: [],
            },
        ],
    },
];
