import React, { lazy } from "react";
import { RouteChildrenProps } from "interface/props";

const _REACT_BASE_URL = window._REACT_BASE_URL;
const LoginPage = lazy(() => import("pages/loginPage/login"));
const LayoutMain = lazy(() => import("layouts/layout"));
const LayoutMainSub = {
    dashboard: lazy(() => import("pages/dashboardPage/dashboard")),
    articleList: lazy(
        () => import("pages/articleGroup/articleListPage/articleList")
    ),
    articleCategory: lazy(
        () => import("pages/articleGroup/articleCategoryPage/articleCategory")
    ),
    articleTag: lazy(
        () => import("pages/articleGroup/articleTagPage/articleTag")
    ),
};

export interface IRoutes {
    path: string;
    key: string;
    component: React.ComponentType<RouteChildrenProps>;
    exact: boolean;
    children?: IRoutes[];
}

/**
 * 所有路由全部处理在这里，含有子页面的 Props 必须继承自 RouteChildrenProps
 * 默认子页面是子页面列表中的第一个
 */
export const Routes: IRoutes[] = [
    {
        path: _REACT_BASE_URL + "login",
        key: "/login",
        component: LoginPage,
        exact: true,
    },
    {
        path: _REACT_BASE_URL,
        key: "/",
        component: LayoutMain,
        exact: false,
        children: [
            {
                path: _REACT_BASE_URL + "dashboard",
                key: "/dashboard",
                component: LayoutMainSub.dashboard,
                exact: true,
            },
            {
                path: _REACT_BASE_URL + "articleList",
                key: "/articleList",
                component: LayoutMainSub.articleList,
                exact: true,
            },
            {
                path: _REACT_BASE_URL + "articleCategory",
                key: "/articleCategory",
                component: LayoutMainSub.articleCategory,
                exact: true,
            },
            {
                path: _REACT_BASE_URL + "articleTag",
                key: "/articleTag",
                component: LayoutMainSub.articleTag,
                exact: true,
            },
        ],
    },
    // 404
];
