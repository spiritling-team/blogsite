import React from "react";
import RouterComponent from "./routers/index";
import { useSelector } from "react-redux";

import { RootState } from "interface/reduxs";
import { Spin } from "antd";
import styles from "./App.module.scss";

export default () => {
    const loading = useSelector((state: RootState) => state.Loading);
    return (
        <React.Fragment>
            <Spin
                className={styles.app_gobal_spin}
                spinning={loading.status}
                tip={"loading....."}>
                <RouterComponent />
            </Spin>
        </React.Fragment>
    );
};
