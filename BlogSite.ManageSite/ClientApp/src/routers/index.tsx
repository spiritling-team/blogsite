import React, { Suspense } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
import { IRoutes, Routes } from "configs/router";
import Loading from "components/LoadingComponent/Loading";

/**
 * @description 含有子页面的进行循环处理
 * @param {IRoutes} route
 * @returns
 */
function NestedPage(route: IRoutes, path: string) {
    return (
        <Route
            path={path}
            render={(props) => (
                <route.component
                    children={
                        <Switch key={path}>
                            {route.children?.map((item) => {
                                if (item.children && item.children.length > 0) {
                                    // 含有子集
                                    return NestedPage(item, item.path);
                                } else {
                                    // 不含有子集
                                    return SinglePage(item);
                                }
                            })}
                            {/* 含有子页面的都进行重定向至第一个子页面 */}
                            {route.children && route.children.length && (
                                <Redirect to={route.children[0].path} />
                            )}
                        </Switch>
                    }
                />
            )}></Route>
    );
}

/**
 * @description 单独页面处理，不含有子页面
 * @param {IRoutes} route
 * @returns
 */
function SinglePage(route: IRoutes) {
    return (
        <Route
            key={route.key}
            path={route.path}
            exact={route.exact}
            component={route.component}
        />
    );
}

export default () => {
    return (
        <React.Fragment>
            <Router>
                <React.Fragment>
                    <Suspense fallback={<Loading />}>
                        <Switch key='root'>
                            {Routes.map((item) => {
                                if (item.children && item.children.length > 0) {
                                    return NestedPage(item, item.path);
                                } else {
                                    return SinglePage(item);
                                }
                            })}
                        </Switch>
                    </Suspense>
                </React.Fragment>
            </Router>
        </React.Fragment>
    );
};
