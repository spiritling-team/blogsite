import _ from "lodash";
import { notification } from "antd";
export const ErrorListDefault = (errorList: { [key: string]: string[] }) => {
    _.forEach(errorList, (value, key) => {
        notification.error({
            message: `Filed Error: ${key} `,
            description: `${value.join("；")}`,
            duration: null,
        });
    });
};

export const errorTips = (tips: { message: string; description: string }) => {
    notification.error({
        message: tips.message,
        description: tips.description,
        duration: null,
    });
};
