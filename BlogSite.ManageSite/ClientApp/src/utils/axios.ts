import axiosOrigin, { AxiosRequestConfig, AxiosResponse } from "axios";
import { v4 as uuid } from "uuid";
import _ from "lodash";
import { message } from "antd";
import { RefreshTokenAsync } from "server/refreshToken";
import { ErrorListDefault } from "./common";

const axios = axiosOrigin.create({
    baseURL: process.env.REACT_APP_AXIOS_BASEURL,
});

axios.defaults.headers.common["Content-Type"] = "application/json";

/**
 * 1. 可以存放请求相关信息
 * 2. 当请求出现异常时可以进行自动重试（主要是为了请求时token过期，自动刷新token，并且重新发起请求）
 */

interface axiosListItem {
    id: string;
    config: AxiosRequestConfig;
}

const axiosList: axiosListItem[] = [];

/**
 * Axios Request 拦截器
 */
axios.interceptors.request.use(
    function (config) {
        // 追加每个请求的特殊ID和config，以便token过期重放请求
        const uid = uuid();
        config.headers["x-uuid"] = uid;
        config.headers["Authorization"] = `Bearer ${
            localStorage.getItem("token") ?? "not token"
        }`;
        axiosList.push({
            id: uid,
            config,
        });
        if (process.env.NODE_ENV.toLocaleLowerCase() === "development") {
            console.log("Request List：", _.cloneDeep(axiosList));
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

/**
 * Axios Response 拦截器
 */
axios.interceptors.response.use(
    async function (response) {
        // 对响应数据做点什么
        // 移除掉请求成功的记录
        if (process.env.NODE_ENV.toLocaleLowerCase() === "development") {
            console.log("Response List After Delete：", _.cloneDeep(axiosList));
        }

        const uid = response.config.headers["x-uuid"];
        _.remove(axiosList, (item) => {
            return item.id === uid;
        });
        if (process.env.NODE_ENV.toLocaleLowerCase() === "development") {
            console.log(
                "Response List Before Delete：",
                _.cloneDeep(axiosList)
            );
        }
        return response;
    },
    async function (error) {
        const response = error.response as AxiosResponse<any>;
        if (response) {
            if (response.status === 701) {
                // 进行重新获取
                var refreshJwtToken = await RefreshTokenAsync();
                // 用户token和刷新token 以及信息存放
                localStorage.setItem(
                    "token",
                    refreshJwtToken.data.data?.jwtToken ?? ""
                );
                localStorage.setItem(
                    "refreshToken",
                    refreshJwtToken.data.data?.refreshToken ?? ""
                );
                localStorage.setItem(
                    "userInfo",
                    JSON.stringify({
                        keyId: refreshJwtToken.data.data?.userKeyId,
                    })
                );
                if (axiosList.length > 0) {
                    var config = axiosList[0].config;
                    config.headers[
                        "Authorization"
                    ] = `Bearer ${refreshJwtToken.data.data?.jwtToken}`;

                    const uid = response.config.headers["x-uuid"];
                    _.remove(axiosList, (item) => {
                        return item.id === uid;
                    });

                    return await axios.request(config);
                } else {
                    return refreshJwtToken;
                }
            } else {
                // 删除保留的接口
                const uid = response.config.headers["x-uuid"];
                _.remove(axiosList, (item) => {
                    return item.id === uid;
                });
                if (response.status === 401) {
                    message.error("登录过期，请重新登录");
                    localStorage.clear();
                    window.location.href = window._REACT_BASE_URL + "login";
                } else if (response.status === 801) {
                    // 数据验证错误
                    const errorResponse = error.response as AxiosResponse<
                        GlobalResultDto<string>
                    >;
                    ErrorListDefault(errorResponse.data.errorList);
                    return Promise.reject(response);
                }
            }
            return Promise.reject(response);
        } else {
            _.remove(axiosList);
            return Promise.reject({
                data: {
                    currentTime: new Date(),
                    message: "Internal Server Error",
                    path: "服务器无法请求",
                    statusCode: 500,
                },
                error,
            });
        }
    }
);

export default axios;
