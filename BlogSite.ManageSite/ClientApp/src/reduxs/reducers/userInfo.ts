import { ReduxTypeEnum } from "enum/reduxs";
import { IUserInfo, IAction } from "interface/reduxs";
import { defaultUserInfo } from "reduxs/store";

export default function UserInfoReducer(
    state: IUserInfo = defaultUserInfo,
    action: IAction<ReduxTypeEnum, IUserInfo>
) {
    switch (action.type) {
        case ReduxTypeEnum.USERINFO:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
