import { ReduxTypeEnum } from "enum/reduxs";
import { ILoading, IAction } from "interface/reduxs";
import { defaultLoading } from "reduxs/store";

export default function LoadingReducer(
    state: ILoading = defaultLoading,
    action: IAction<ReduxTypeEnum, ILoading>
) {
    switch (action.type) {
        case ReduxTypeEnum.LOADING:
            return { ...state, ...action.payload };
        default:
            return state;
    }
}
