import { createStore, combineReducers, applyMiddleware } from "redux";
import logger from "redux-logger";
import promiseMiddleware from "redux-promise";
import thunk from "redux-thunk";

import LoadingReducer from "reduxs/reducers/loading";
import UserInfoReducer from "reduxs/reducers/userInfo";

const Reducers = {
    Loading: LoadingReducer,
    UserInfo: UserInfoReducer,
};

// 2 日志
//3、存入仓库
export default createStore(
    combineReducers(Reducers),
    applyMiddleware(thunk, promiseMiddleware, logger)
);

/**
 * const loading = useSelector((state: RootState) => state.Loading);
 * const dispatch = useDispatch<Dispatch<IAction<ReduxTypeEnum, ILoading>>>();
 * dispatch({
        type: ReduxTypeEnum.LOADING,
        payload: { status: true },
    });
 */
