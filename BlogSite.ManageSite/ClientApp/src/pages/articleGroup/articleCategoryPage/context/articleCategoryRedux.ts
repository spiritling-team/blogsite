import React, { useContext, useReducer } from "react";
import { ArticleCategoryReduxType } from "../utils/articleCategory.interface";

// ArticleCategoryFormModal相关配置
interface IArticleCategoryFormModal {
    Destroy?: boolean;
    Status?: boolean;
    IsEdit?: boolean;
    CategoryId?: number;
}

// 默认值
let defaultFormModalAbout: IArticleCategoryFormModal = {
    Destroy: false,
    Status: false,
    IsEdit: false,
    CategoryId: 0,
};

const ArticleCategoryFormModalAboutContext = React.createContext(
    {} as StateContext<ArticleCategoryReduxType, IArticleCategoryFormModal>
);

let ArticleCategoryFormModalAboutReducer = function (
    state: IArticleCategoryFormModal,
    action: IPartialAction<ArticleCategoryReduxType, IArticleCategoryFormModal>
) {
    switch (action.type) {
        case ArticleCategoryReduxType.FormModalStatus:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};

export default {
    defaultFormModalAbout,
    ArticleCategoryFormModalAboutContext,
    ArticleCategoryFormModalAboutReducer,
    ArticleCategoryReduxType,
};
