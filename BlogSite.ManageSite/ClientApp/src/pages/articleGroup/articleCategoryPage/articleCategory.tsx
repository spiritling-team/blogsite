import React, { useState, useReducer } from "react";
import { Space, Button, message } from "antd";
import styles from "./articleCategory.module.scss";
import { useBoolean } from "hooks";

import ArticleCategoryRedux from "./context/articleCategoryRedux";
import ArticleCategoryFormModal from "./components/articleCategoryFormModal";
import ArticleCategoryListTable from "./components/articleCategoryListTable";

const ArticleCategoryContext =
    ArticleCategoryRedux.ArticleCategoryFormModalAboutContext;
export default () => {
    const [stateFormModalAbout, dispatchFormModalAbout] = useReducer(
        ArticleCategoryRedux.ArticleCategoryFormModalAboutReducer,
        ArticleCategoryRedux.defaultFormModalAbout
    );
    const [refresh, setRefresh] = useBoolean(true);
    const handleNewCategory = function () {
        dispatchFormModalAbout({
            type: ArticleCategoryRedux.ArticleCategoryReduxType.FormModalStatus,
            payload: {
                Status: true,
                IsEdit: false,
                Destroy: true,
            },
        });
    };
    const handleEditCategory = function (id: number) {
        console.log(id);
        dispatchFormModalAbout({
            type: ArticleCategoryRedux.ArticleCategoryReduxType.FormModalStatus,
            payload: {
                Status: true,
                IsEdit: true,
                Destroy: true,
                CategoryId: id,
            },
        });
    };
    const handleRefershList = function () {
        setRefresh.setFalse();
        setRefresh.setTrue();
    };
    return (
        <section className={styles.articleCategory}>
            <div className={styles.navHeader}>
                <Space>
                    <Button type='primary' onClick={handleNewCategory}>
                        新建
                    </Button>
                </Space>
            </div>
            <div className={styles.content}>
                {refresh && (
                    <ArticleCategoryListTable
                        onEdit={handleEditCategory}
                        onRefersh={handleRefershList}
                    />
                )}
            </div>
            <ArticleCategoryContext.Provider
                value={{
                    state: stateFormModalAbout,
                    setState: dispatchFormModalAbout,
                }}>
                {stateFormModalAbout.Destroy && (
                    <ArticleCategoryFormModal
                        handleRefershList={handleRefershList}
                    />
                )}
            </ArticleCategoryContext.Provider>
        </section>
    );
};
