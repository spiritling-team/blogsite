import axios from "utils/axios";
import { AxiosResponse } from "axios";
import { IFormData, IListTable } from "../utils/articleCategory.interface";

export const articleCategoryPostServer = async function (
    value: IFormData
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = "articleCategory";
    return await axios.post(url, value);
};

export const articleCategoryDelete4Id = async function (
    id: number
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = `articleCategory/${id}`;
    return await axios.delete(url);
};

export const articleCategoryPut4Id = async function (
    data: IListTable
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = `articleCategory`;
    return await axios.put(url, data);
};

export const articleCategoryGetListTable = async function (
    coditionEntityDefault: CoditionEntity
): Promise<
    AxiosResponse<
        GlobalResultDto<{ data: IListTable[]; coditionEntity: CoditionEntity }>
    >
> {
    var url = "articleCategory";
    return await axios.get(url, {
        params: { codingtionStr: JSON.stringify(coditionEntityDefault) },
    });
};

export const articleCategoryGet4Id = async function (
    id: number
): Promise<AxiosResponse<GlobalResultDto<IListTable>>> {
    var url = `articleCategory/${id}`;
    return await axios.get(url);
};
