export interface IFormData {
    name: string;
    urlParam: string;
}

// action类型枚举
export enum ArticleCategoryReduxType {
    FormModalStatus = 0 << 1,
}

export interface IListTable extends IFormData {
    id: number;
    keyId: string;
    createdTime: Date;
    updatedTime: Date;
}
