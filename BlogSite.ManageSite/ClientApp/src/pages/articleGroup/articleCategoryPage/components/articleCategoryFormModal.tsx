import React, { useContext, useEffect, useState, Dispatch } from "react";
import {
    Modal,
    Form,
    Input,
    Tooltip,
    Button,
    Space,
    message,
    Spin,
} from "antd";
import { createFromIconfontCN } from "@ant-design/icons";
import cryptoRandomString from "crypto-random-string";

import { IFormData, IListTable } from "../utils/articleCategory.interface";
import ArticleCategoryRedux from "../context/articleCategoryRedux";
import styles from "./articleCategoryFormModal.module.scss";
import {
    articleCategoryPostServer,
    articleCategoryGet4Id,
    articleCategoryPut4Id,
} from "../server/articleCategory.server";
import { useBoolean } from "hooks";
import { errorTips } from "utils/common";

const { useForm } = Form;
const IconFont = createFromIconfontCN({
    scriptUrl: window._ICON_FONT_URL,
});

const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
};

const getArticleCategory4Id = async function (id: number): Promise<IListTable> {
    try {
        var result = await articleCategoryGet4Id(id);
        if (result.data.data) {
            return result.data.data;
        }
        return {} as IListTable;
    } catch (e) {
        errorTips({
            message: "Get Article Categorys For Id Error",
            description: JSON.stringify(e),
        });
        return {} as IListTable;
    }
};

export interface Props {
    handleRefershList: () => void;
}

export default (props: Props) => {
    const { state, setState } = useContext(
        ArticleCategoryRedux.ArticleCategoryFormModalAboutContext
    );
    const [form] = useForm<IFormData>();
    const [editInfo, setEditInfo] = useState<IListTable>();

    const [loading, setLoading] = useBoolean(false);

    const [name, setName] = useState("");
    const [urlParam, setUrlParam] = useState("");

    useEffect(function () {
        let init = async function () {
            if (state.IsEdit && state.CategoryId) {
                setLoading.setTrue();
                // 需要获取编辑数据
                var result = await getArticleCategory4Id(state.CategoryId);
                setEditInfo(result);
                setName(result.name);
                setUrlParam(result.urlParam);
                // updateFormFileds("name", result.name);
                // updateFormFileds("bgColor", result.bgColor);
                // updateFormFileds("textColor", result.textColor);
                // updateFormFileds("urlParam", result.urlParam);
                form.setFieldsValue(result);
                setLoading.setFalse();
            }
        };
        init();
    }, []);

    // 新规创建数据
    const PostServerAsync = async function name(formData: IFormData) {
        try {
            await articleCategoryPostServer(formData);
            message.success("保存成功");
            closeModal();
            props.handleRefershList();
        } catch (e) {
            console.log(e);
            errorTips({
                message: "Created Post Error",
                description: JSON.stringify(e),
            });
        } finally {
            setLoading.setFalse();
        }
    };

    const PutServerAsync = async function name(data: IFormData) {
        var b = data as IListTable;
        b.id = editInfo?.id ?? 0;
        try {
            await articleCategoryPut4Id(b);
            message.success("更新成功");
            closeModal();
            props.handleRefershList();
        } catch (e) {
            errorTips({
                message: "Edit Put Error",
                description: JSON.stringify(e),
            });
        } finally {
            setLoading.setFalse();
        }
    };

    // 确定函数执行
    const handleOk = async function () {
        setLoading.setTrue();
        // 验证form自带验证是否通过
        try {
            var formData = await form.validateFields();
            if (state.IsEdit) {
                await PutServerAsync(formData as IFormData);
            } else {
                await PostServerAsync(formData as IFormData);
            }
        } catch (e) {
            console.log(e);
        } finally {
            setLoading.setFalse();
        }
    };

    const handleRadomUrl = function () {
        const url = cryptoRandomString({ length: 15, type: "url-safe" });
        setUrlParam(url);
        updateFormFileds("urlParam", url);
        form.validateFields(["urlParam"]);
    };

    /**
     * @description 需要手动将Form值添加进Form中
     */
    function updateFormFileds(key: keyof IFormData, v: string) {
        let value = form.getFieldsValue() as IFormData;
        value[key] = v;
        form.setFieldsValue(value);
    }

    /**
     * @description 通过Redux关闭Modal
     */
    function closeModal() {
        setState({
            type: ArticleCategoryRedux.ArticleCategoryReduxType.FormModalStatus,
            payload: {
                Status: false,
            },
        });
    }

    /**
     * @description 通过Redux销毁Modal
     */
    function destoryModal() {
        setState({
            type: ArticleCategoryRedux.ArticleCategoryReduxType.FormModalStatus,
            payload: {
                Destroy: false,
            },
        });
    }
    return (
        <Modal
            title={state.IsEdit ? "编辑分类" : "新建分类"}
            visible={state.Status}
            onOk={handleOk}
            maskClosable={false}
            onCancel={closeModal}
            afterClose={destoryModal}
            confirmLoading={loading}>
            <Spin spinning={loading}>
                <Form
                    form={form}
                    {...layout}
                    initialValues={{
                        bgColor: "#000000",
                        textColor: "#FFFFFF",
                    }}>
                    <Form.Item
                        label='名称'
                        name='name'
                        rules={[
                            {
                                required: true,
                            },
                            {
                                max: 30,
                            },
                        ]}>
                        <Input
                            disabled={loading}
                            onChange={(e) => {
                                setName(e.target.value);
                            }}
                            allowClear
                        />
                    </Form.Item>
                    <Form.Item label='访问地址'>
                        <Space>
                            <Form.Item
                                name='urlParam'
                                label={"访问地址"}
                                noStyle
                                rules={[{ max: 30 }]}>
                                <Input
                                    disabled={loading}
                                    onChange={(e) => {
                                        if (!loading) {
                                            setUrlParam(e.target.value);
                                        }
                                    }}
                                    allowClear
                                />
                            </Form.Item>
                            <Tooltip placement={"top"} title={"随机访问地址"}>
                                <Button
                                    onClick={handleRadomUrl}
                                    icon={<IconFont type={"bs_suiji"} />}
                                />
                            </Tooltip>
                        </Space>
                    </Form.Item>
                    <Form.Item
                        label='备注'
                        name='remark'
                        rules={[{ max: 255 }]}>
                        <Input.TextArea rows={5} disabled={loading} />
                    </Form.Item>
                </Form>
            </Spin>
        </Modal>
    );
};
