import React, { useContext, useReducer } from "react";
import { ArticleTagReduxType } from "../utils/articleTag.interface";

// ArticleTagFormModal相关配置
interface IArticleTagFormModal {
    Destroy?: boolean;
    Status?: boolean;
    IsEdit?: boolean;
    TagId?: number;
}

// 默认值
let defaultFormModalAbout: IArticleTagFormModal = {
    Destroy: false,
    Status: false,
    IsEdit: false,
    TagId: 0,
};

const ArticleTagFormModalAboutContext = React.createContext(
    {} as StateContext<ArticleTagReduxType, IArticleTagFormModal>
);

let ArticleTagFormModalAboutReducer = function (
    state: IArticleTagFormModal,
    action: IPartialAction<ArticleTagReduxType, IArticleTagFormModal>
) {
    switch (action.type) {
        case ArticleTagReduxType.FormModalStatus:
            return { ...state, ...action.payload };
        default:
            return state;
    }
};

export default {
    defaultFormModalAbout,
    ArticleTagFormModalAboutContext,
    ArticleTagFormModalAboutReducer,
    ArticleTagReduxType,
};
