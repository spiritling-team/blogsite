import React, { useContext, useEffect, useState, Dispatch } from "react";
import {
    Modal,
    Form,
    Input,
    Tooltip,
    Tag,
    Button,
    Space,
    message,
    Spin,
} from "antd";
import { createFromIconfontCN } from "@ant-design/icons";
import { SketchPicker, ColorResult } from "react-color";
import cryptoRandomString, { async } from "crypto-random-string";

import { IFormData, IListTable } from "../utils/articleTag.interface";
import ArticleTagRedux from "../context/articleTagRedux";
import styles from "./articleTagFormModal.module.scss";
import {
    articleTagPostServer,
    articleTagGet4Id,
    articleTagPut4Id,
} from "../server/articleTag.server";
import { useBoolean } from "hooks";
import { errorTips } from "utils/common";

const { useForm } = Form;
const IconFont = createFromIconfontCN({
    scriptUrl: window._ICON_FONT_URL,
});

const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 18 },
};

const getArticleTag4Id = async function (id: number): Promise<IListTable> {
    try {
        var result = await articleTagGet4Id(id);
        if (result.data.data) {
            return result.data.data;
        }
        return {} as IListTable;
    } catch (e) {
        errorTips({
            message: "Get Article Tags For Id Error",
            description: JSON.stringify(e),
        });
        return {} as IListTable;
    }
};

export interface Props {
    handleRefershList: () => void;
}

export default (props: Props) => {
    const { state, setState } = useContext(
        ArticleTagRedux.ArticleTagFormModalAboutContext
    );
    const [form] = useForm<IFormData>();
    const [editInfo, setEditInfo] = useState<IListTable>();

    const [loading, setLoading] = useBoolean(false);

    const [name, setName] = useState("");
    const [bgColor, setBgColor] = useState("#000000");
    const [textColor, setTextColor] = useState("#FFFFFF");
    const [urlParam, setUrlParam] = useState("");

    useEffect(function () {
        let init = async function () {
            if (state.IsEdit && state.TagId) {
                setLoading.setTrue();
                // 需要获取编辑数据
                var result = await getArticleTag4Id(state.TagId);
                setEditInfo(result);
                setName(result.name);
                setBgColor(result.bgColor);
                setTextColor(result.textColor);
                setUrlParam(result.urlParam);
                // updateFormFileds("name", result.name);
                // updateFormFileds("bgColor", result.bgColor);
                // updateFormFileds("textColor", result.textColor);
                // updateFormFileds("urlParam", result.urlParam);
                form.setFieldsValue(result);
                setLoading.setFalse();
            }
        };
        init();
    }, []);

    // 新规创建数据
    const PostServerAsync = async function name(formData: IFormData) {
        try {
            await articleTagPostServer(formData);
            message.success("保存成功");
            closeModal();
            props.handleRefershList();
        } catch (e) {
            console.log(e);
            errorTips({
                message: "Created Post Error",
                description: JSON.stringify(e),
            });
        } finally {
            setLoading.setFalse();
        }
    };

    const PutServerAsync = async function name(data: IFormData) {
        var b = data as IListTable;
        b.id = editInfo?.id ?? 0;
        try {
            await articleTagPut4Id(b);
            message.success("更新成功");
            closeModal();
            props.handleRefershList();
        } catch (e) {
            errorTips({
                message: "Edit Put Error",
                description: JSON.stringify(e),
            });
        } finally {
            setLoading.setFalse();
        }
    };

    // 确定函数执行
    const handleOk = async function () {
        setLoading.setTrue();
        // 验证form自带验证是否通过
        try {
            var formData = await form.validateFields();
            if (state.IsEdit) {
                await PutServerAsync(formData as IFormData);
            } else {
                await PostServerAsync(formData as IFormData);
            }
        } catch (e) {
            console.log(e);
        } finally {
            setLoading.setFalse();
        }
    };

    const handleBgColorChange = function (colorResult: ColorResult) {
        if (!loading) {
            setBgColor(colorResult.hex);
            updateFormFileds("bgColor", colorResult.hex);
        }
    };
    const handleTextColorChange = function (colorResult: ColorResult) {
        if (!loading) {
            setTextColor(colorResult.hex);
            updateFormFileds("textColor", colorResult.hex);
        }
    };
    const handleRadomUrl = function () {
        const url = cryptoRandomString({ length: 15, type: "url-safe" });
        setUrlParam(url);
        updateFormFileds("urlParam", url);
        form.validateFields(["urlParam"]);
    };

    /**
     * @description 需要手动将Form值添加进Form中
     */
    function updateFormFileds(key: keyof IFormData, v: string) {
        let value = form.getFieldsValue() as IFormData;
        value[key] = v;
        form.setFieldsValue(value);
    }

    /**
     * @description 通过Redux关闭Modal
     */
    function closeModal() {
        setState({
            type: ArticleTagRedux.ArticleTagReduxType.FormModalStatus,
            payload: {
                Status: false,
            },
        });
    }

    /**
     * @description 通过Redux销毁Modal
     */
    function destoryModal() {
        setState({
            type: ArticleTagRedux.ArticleTagReduxType.FormModalStatus,
            payload: {
                Destroy: false,
            },
        });
    }
    return (
        <Modal
            title={state.IsEdit ? "编辑标签" : "新建标签"}
            visible={state.Status}
            onOk={handleOk}
            maskClosable={false}
            onCancel={closeModal}
            afterClose={destoryModal}
            confirmLoading={loading}>
            <Spin spinning={loading}>
                <Form
                    form={form}
                    {...layout}
                    initialValues={{
                        bgColor: "#000000",
                        textColor: "#FFFFFF",
                    }}>
                    <Form.Item
                        label='名称'
                        name='name'
                        rules={[
                            {
                                required: true,
                            },
                            {
                                max: 30,
                            },
                        ]}>
                        <Input
                            disabled={loading}
                            onChange={(e) => {
                                setName(e.target.value);
                            }}
                            allowClear
                        />
                    </Form.Item>
                    <Form.Item label='背景颜色' name='bgColor'>
                        <Tooltip
                            placement='bottomLeft'
                            trigger={"click"}
                            title={
                                <div className={styles.colorPicker}>
                                    <SketchPicker
                                        color={bgColor}
                                        disableAlpha={false}
                                        onChange={handleBgColorChange}
                                    />
                                </div>
                            }>
                            <div className={styles.colorPickerWarp}>
                                <div
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        backgroundColor: bgColor,
                                    }}></div>
                            </div>
                        </Tooltip>
                    </Form.Item>
                    <Form.Item label='文字颜色' name='textColor'>
                        <Tooltip
                            placement='bottomLeft'
                            trigger={"click"}
                            title={
                                <div className={styles.colorPicker}>
                                    <SketchPicker
                                        color={textColor}
                                        disableAlpha={false}
                                        onChange={handleTextColorChange}
                                    />
                                </div>
                            }>
                            <div className={styles.colorPickerWarp}>
                                <div
                                    style={{
                                        width: "100%",
                                        height: "100%",
                                        backgroundColor: textColor,
                                    }}></div>
                            </div>
                        </Tooltip>
                    </Form.Item>
                    <Form.Item label='展示标签' name='showTag'>
                        <Tag color={bgColor} style={{ color: textColor }}>
                            {!name ? "标签" : name}
                        </Tag>
                    </Form.Item>
                    <Form.Item label='访问地址'>
                        <Space>
                            <Form.Item
                                name='urlParam'
                                label={"访问地址"}
                                noStyle
                                rules={[{ max: 30 }]}>
                                <Input
                                    disabled={loading}
                                    onChange={(e) => {
                                        if (!loading) {
                                            setUrlParam(e.target.value);
                                        }
                                    }}
                                    allowClear
                                />
                            </Form.Item>
                            <Tooltip placement={"top"} title={"随机访问地址"}>
                                <Button
                                    onClick={handleRadomUrl}
                                    icon={<IconFont type={"bs_suiji"} />}
                                />
                            </Tooltip>
                        </Space>
                    </Form.Item>
                    <Form.Item
                        label='备注'
                        name='remark'
                        rules={[{ max: 255 }]}>
                        <Input.TextArea rows={5} disabled={loading} />
                    </Form.Item>
                </Form>
            </Spin>
        </Modal>
    );
};
