import React, { useEffect, useState } from "react";
import { Table, Button, Space, Modal, message } from "antd";
import { ExclamationCircleOutlined, EyeOutlined } from "@ant-design/icons";
import { ColumnsType, TablePaginationConfig } from "antd/es/table";
import { IListTable } from "../utils/articleTag.interface";
import {
    articleTagGetListTable,
    articleTagDelete4Id,
} from "../server/articleTag.server";
import { useBoolean } from "hooks";
import { TagShow } from "components";
import { errorTips } from "utils/common";
import dayjs from "dayjs";
import { produce } from "immer";
import _ from "lodash";

const coditionEntityDefault: CoditionEntity = {
    paginationKeyValue: {
        enable: true,
        count: 0,
        pageNumber: 1,
        pageSize: 10,
    },
    orderByKeyValue: {
        enable: true,
        propertyName: "createdTime",
        orderDirection: 1,
    },
    searchKeyValue: {
        enable: true,
        propertyName: "name",
        searchContent: "",
    },
};

export interface Props {
    onEdit: (id: number) => void;
    onRefersh: () => void;
}

export default (props: Props) => {
    const [coditionEntity, setCoditionEntity] = useState<CoditionEntity>(
        coditionEntityDefault
    );
    const [list, setList] = useState<IListTable[]>([]);
    const [loading, setLoading] = useBoolean(true);
    async function GetListAsync(codition: CoditionEntity = coditionEntity) {
        setLoading.setTrue();
        var result = await articleTagGetListTable(codition);
        try {
            if (result.data.data && result.data.data.data.length > 0) {
                setList(result.data.data.data);
                setCoditionEntity(result.data.data.coditionEntity);
            }
        } catch (err) {
            errorTips({
                message: "Article Tag Get List Table",
                description: JSON.stringify(err),
            });
        }
        setLoading.setFalse();
    }
    useEffect(function () {
        var init = async function () {
            await GetListAsync();
        };
        init();
    }, []);

    async function TableChange(pagination: TablePaginationConfig, sorter: any) {
        console.log(pagination, sorter);
        var sort = sorter as {
            columnKey: string;
            field: string;
            order: string;
        };
        var next = produce(coditionEntity, (draft) => {
            draft.orderByKeyValue.enable = true;
            draft.orderByKeyValue.propertyName = sort.columnKey;
            switch (sorter.order) {
                case "descend":
                    draft.orderByKeyValue.orderDirection = 1;
                    break;
                case "ascend":
                    draft.orderByKeyValue.orderDirection = 0;
                    break;
                default:
                    draft.orderByKeyValue.enable = false;
            }
            draft.paginationKeyValue.pageNumber = pagination.current || 1;
            draft.paginationKeyValue.pageSize = pagination.pageSize || 10;
        });
        await GetListAsync(next);
    }

    const deleteItem = async function (id: number) {
        Modal.confirm({
            title: "删除提示",
            icon: <ExclamationCircleOutlined />,
            content: "是否删除这条记录？",
            okType: "danger",
            onOk: async () => {
                setLoading.setTrue();
                try {
                    var result = await articleTagDelete4Id(id);
                    message.success("删除成功");
                    await GetListAsync();
                } catch (err) {
                    errorTips({
                        message: "ArticleTag Delete Eorro",
                        description: JSON.stringify(err),
                    });
                }
                setLoading.setFalse();
                props.onRefersh();
            },
            onCancel() {},
        });
    };

    const columns: ColumnsType<IListTable> = [
        {
            title: "标签名",
            dataIndex: "name",
            key: "name",
            sorter: true,
        },
        {
            title: "KeyId",
            dataIndex: "keyId",
            key: "keyId",
        },
        {
            title: "url地址",
            dataIndex: "urlParam",
            key: "urlParam",
        },
        {
            title: "Tag",
            render: (v, record, index) => {
                return (
                    <TagShow
                        key={index}
                        text={record.name}
                        textColor={record.textColor}
                        bgColor={record.bgColor}
                    />
                );
            },
        },
        {
            title: "创建时间",
            key: "createdTime",
            dataIndex: "createdTime",
            render: (v) => `${dayjs(v).format("YYYY-MM-DD HH:mm:ss")}`,
            sorter: true,
            defaultSortOrder: "descend",
        },
        {
            title: "备注",
            key: "remark",
            dataIndex: "remark",
            render: (v) => (
                <Button
                    disabled={_.isEmpty(v) ? true : false}
                    onClick={() => {
                        Modal.info({
                            title: "备注",
                            content: v,
                        });
                    }}
                    icon={<EyeOutlined />}
                />
            ),
        },
        {
            title: "操作",
            render: (value, record) => {
                return (
                    <Space>
                        <Button onClick={() => props.onEdit(record.id)}>
                            编辑
                        </Button>
                        <Button danger onClick={() => deleteItem(record.id)}>
                            删除
                        </Button>
                    </Space>
                );
            },
        },
    ];
    return (
        <div>
            <Table
                columns={columns}
                dataSource={list}
                bordered
                loading={loading}
                pagination={{
                    pageSize: coditionEntity.paginationKeyValue.pageSize,
                    current: coditionEntity.paginationKeyValue.pageNumber,
                    total: coditionEntity.paginationKeyValue.count,
                    pageSizeOptions: ["10", "20", "50", "100"],
                    showSizeChanger: true,
                }}
                onChange={(pagination, filter, sorter) =>
                    TableChange(pagination, sorter)
                }
            />
        </div>
    );
};
