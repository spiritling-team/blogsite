import React, { useState, useReducer } from "react";
import { Space, Button, message } from "antd";
import styles from "./articleTag.module.scss";
import { useBoolean } from "hooks";

import ArticleTagRedux from "./context/articleTagRedux";
import ArticleTagFormModal from "./components/articleTagFormModal";
import ArticleTagListTable from "./components/articleTagListTable";

const ArticleTagContext = ArticleTagRedux.ArticleTagFormModalAboutContext;
export default () => {
    const [stateFormModalAbout, dispatchFormModalAbout] = useReducer(
        ArticleTagRedux.ArticleTagFormModalAboutReducer,
        ArticleTagRedux.defaultFormModalAbout
    );
    const [refresh, setRefresh] = useBoolean(true);
    const handleNewTag = function () {
        dispatchFormModalAbout({
            type: ArticleTagRedux.ArticleTagReduxType.FormModalStatus,
            payload: {
                Status: true,
                IsEdit: false,
                Destroy: true,
            },
        });
    };
    const handleEditTag = function (id: number) {
        console.log(id);
        dispatchFormModalAbout({
            type: ArticleTagRedux.ArticleTagReduxType.FormModalStatus,
            payload: {
                Status: true,
                IsEdit: true,
                Destroy: true,
                TagId: id,
            },
        });
    };
    const handleRefershList = function () {
        setRefresh.setFalse();
        setRefresh.setTrue();
    };
    return (
        <section className={styles.articleTag}>
            <div className={styles.navHeader}>
                <Space>
                    <Button type='primary' onClick={handleNewTag}>
                        新建
                    </Button>
                </Space>
            </div>
            <div className={styles.content}>
                {refresh && (
                    <ArticleTagListTable
                        onEdit={handleEditTag}
                        onRefersh={handleRefershList}
                    />
                )}
            </div>
            <ArticleTagContext.Provider
                value={{
                    state: stateFormModalAbout,
                    setState: dispatchFormModalAbout,
                }}>
                {stateFormModalAbout.Destroy && (
                    <ArticleTagFormModal
                        handleRefershList={handleRefershList}
                    />
                )}
            </ArticleTagContext.Provider>
        </section>
    );
};
