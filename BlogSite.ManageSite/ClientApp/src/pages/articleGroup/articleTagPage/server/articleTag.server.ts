import axios from "utils/axios";
import { AxiosResponse } from "axios";
import { IFormData, IListTable } from "../utils/articleTag.interface";

export const articleTagPostServer = async function (
    value: IFormData
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = "articleTag";
    return await axios.post(url, value);
};

export const articleTagDelete4Id = async function (
    id: number
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = `articleTag/${id}`;
    return await axios.delete(url);
};

export const articleTagPut4Id = async function (
    data: IListTable
): Promise<AxiosResponse<GlobalResultDto<string>>> {
    var url = `articleTag`;
    return await axios.put(url, data);
};

export const articleTagGetListTable = async function (
    coditionEntityDefault: CoditionEntity
): Promise<
    AxiosResponse<
        GlobalResultDto<{ data: IListTable[]; coditionEntity: CoditionEntity }>
    >
> {
    var url = "articleTag";
    return await axios.get(url, {
        params: { codingtionStr: JSON.stringify(coditionEntityDefault) },
    });
};

export const articleTagGet4Id = async function (
    id: number
): Promise<AxiosResponse<GlobalResultDto<IListTable>>> {
    var url = `articleTag/${id}`;
    return await axios.get(url);
};
