import React, { useEffect } from "react";
import axios from "utils/axios";
import { useBoolean } from "hooks";

export default () => {
    const [state, { toggle, setTrue, setFalse }] = useBoolean(false);
    useEffect(function () {
        axios
            .get("WeatherForecast")
            .then((res) => {
                console.log(res);
            })
            .catch((res) => {
                console.log(res);
            });
    }, []);
    return (
        <div>
            <span>Dashboard</span>
        </div>
    );
};
