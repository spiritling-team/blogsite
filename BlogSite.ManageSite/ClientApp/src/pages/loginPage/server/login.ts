import axios from "utils/axios";
import { formValue } from "../utils/login";
import { AxiosResponse } from "axios";
import { ReturnDTO } from "../utils/login";

export const loginPostServer = async function (
    value: formValue
): Promise<AxiosResponse<GlobalResultDto<ReturnDTO.ILoginJwtToken>>> {
    var url = "user/login";
    return await axios.post(url, value);
};
