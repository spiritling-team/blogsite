import React, { useCallback, useState } from "react";
import styles from "./login.module.scss";
import { Typography, Form, Input, Button, message } from "antd";
import zhCN from "./locale/zh_CN.json";
import { formValue } from "./utils/login";
import { loginPostServer } from "./server/login";
import { useHistory } from "react-router-dom";

const { Title } = Typography;
const { useForm } = Form;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};

export default () => {
    const [submitBtnStatus, setSubmitBtnStatus] = useState<boolean>(false);
    const history = useHistory();
    const [form] = useForm<formValue>();

    const loginFinish = async function (value: formValue) {
        setSubmitBtnStatus(true);
        try {
            const serverValue = await loginPostServer(value);
            if (serverValue.data.data?.jwtToken) {
                // 存放到localStorage中
                localStorage.setItem("token", serverValue.data.data?.jwtToken);
                localStorage.setItem(
                    "refreshToken",
                    serverValue.data.data?.refreshToken
                );
                localStorage.setItem(
                    "userInfo",
                    JSON.stringify({
                        keyId: serverValue.data.data?.userKeyId,
                    })
                );
                // 延迟跳转到仪表板页面
                setTimeout(function () {
                    history.push(window._REACT_BASE_URL + "dashboard");
                }, 300);
            } else {
            }
        } catch {
            message.error("登录失败，请重新尝试");
        } finally {
            setSubmitBtnStatus(false);
        }
    };

    return (
        <div className={styles.loginWarp}>
            <div className={styles.loginMain}>
                <Title className={styles.title} level={1}>
                    登&ensp;录
                </Title>
                <Form
                    className={styles.loginForm}
                    form={form}
                    {...layout}
                    onFinish={loginFinish}>
                    <Form.Item
                        name='username'
                        label='用户名'
                        rules={[{ required: true }]}>
                        <Input
                            placeholder={zhCN.LOGIN_FORM_USERNAME_PLACEHOLDER}
                        />
                    </Form.Item>
                    <Form.Item
                        name='password'
                        label='密码'
                        rules={[{ required: true }]}>
                        <Input.Password
                            placeholder={zhCN.LOGIN_FORM_PASSWORD_PLACEHOLDER}
                        />
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button
                            type='primary'
                            htmlType='submit'
                            loading={submitBtnStatus}>
                            登录
                        </Button>
                    </Form.Item>
                </Form>
            </div>
        </div>
    );
};
