import { IUserJwtToken } from "interface/server";

export interface formValue {
    username: string;
    password: string;
}

export namespace ReturnDTO {
    export interface ILoginJwtToken extends IUserJwtToken {}
}
