import axios from "utils/axios";
import { RefreshTokenData, IUserJwtToken } from "interface/server";
import { AxiosResponse } from "axios";

export const RefreshTokenAsync = async function (): Promise<
    AxiosResponse<GlobalResultDto<IUserJwtToken>>
> {
    const path = "user/RefreshToken";
    const data: RefreshTokenData = {
        refreshToken: localStorage.getItem("refreshToken") ?? "",
        keyId: JSON.parse(localStorage.getItem("userInfo") ?? "{}")?.keyId,
    };
    return await axios.post<GlobalResultDto<IUserJwtToken>>(path, data);
};
