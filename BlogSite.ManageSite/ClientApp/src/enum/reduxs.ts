export enum ReduxTypeEnum {
    USERINFO = 1 << 0,
    LOADING = 1 << 1,
}
declare global {
    enum OrderDirection {
        ASC = 0,
        DESC = 1,
    }
}
