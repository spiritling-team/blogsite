export interface IUserInfo {
    password: string;
}

export interface ILoading {
    status: boolean;
}

export interface IAction<T, M> {
    type: T;
    payload: M;
}

export interface RootState {
    UserInfo: IUserInfo;
    Loading: ILoading;
}
