export interface IDebounceOptions {
    wait?: number;
    leading?: boolean;
    trailing?: boolean;
}
export interface IThrottleOptions {
    wait?: number;
    leading?: boolean;
    trailing?: boolean;
}
