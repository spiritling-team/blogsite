import { ReactNode } from "react";
export interface RouteChildrenProps {
    children?: ReactNode;
}
