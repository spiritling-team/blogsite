/**
 * @description 重新刷新Token返回DTO
 */
export interface RefreshTokenData {
    /**
     * @description 刷新Token
     */
    refreshToken: string;
    /**
     * @description 用户Key
     */
    keyId: string;
}

export interface IUserJwtToken {
    /**
     * @description jwtToken
     */
    jwtToken: string;
    /**
     * @description 刷新Token
     */
    refreshToken: string;
    /**
     * @description 用户Key
     */
    userKeyId: string;
}
