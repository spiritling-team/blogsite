import React from "react";
import { Layout, Menu } from "antd";
import styles from "./headerNav.module.scss";

const { Header } = Layout;

export default () => {
    return (
        <Header className={styles.headerNav}>
            <div className={styles.headerTitle}>SpiritLing</div>
            {/* <Menu
                className={styles.leftNav}
                theme='dark'
                mode='horizontal'
                defaultSelectedKeys={["1"]}>
                <Menu.Item key='1'>博客管理</Menu.Item>
                <Menu.Item key='2'>文档管理</Menu.Item>
                <Menu.Item key='3'>资源管理</Menu.Item>
                <Menu.Item key='4'>短网址管理</Menu.Item>
            </Menu> */}
        </Header>
    );
};
