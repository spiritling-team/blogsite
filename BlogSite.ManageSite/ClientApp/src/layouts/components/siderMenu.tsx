import React, { useEffect, useState } from "react";
import { Layout, Menu } from "antd";
import { Link, useLocation, useHistory } from "react-router-dom";
import { createFromIconfontCN } from "@ant-design/icons";
import styles from "./siderMenu.module.scss";
import { IMenuItem, MenuItem } from "configs/menu";
import { Location } from "history";
import _ from "lodash";
const { Sider } = Layout;
const { SubMenu } = Menu;
const IconFont = createFromIconfontCN({
    scriptUrl: window._ICON_FONT_URL,
});

/**
 * @description 含有子集的菜单栏
 * @param {IMenuItem} item
 * @returns
 */
function NestedMenu(item: IMenuItem) {
    return (
        <SubMenu
            key={item.key}
            icon={item.icon ? <IconFont type={item.icon} /> : null}
            title={item.title}>
            {item.children.map((item) => {
                if (item.children.length > 0) {
                    return NestedMenu(item);
                } else {
                    return SingleMenu(item);
                }
            })}
        </SubMenu>
    );
}

/**
 * @description 单个
 * @param {IMenuItem} item
 * @returns
 */
function SingleMenu(item: IMenuItem) {
    return (
        <Menu.Item key={item.key}>
            {item.path && (
                <Link to={item.path}>
                    {item.icon && <IconFont type={item.icon} />}
                    {item.title}
                </Link>
            )}
        </Menu.Item>
    );
}

/**
 * @description 获取打开的key值
 */
function OpenKeys(
    menuItems: IMenuItem[],
    pathname: string,
    parentKey?: string
): string[] {
    let arr: string[] = [];
    menuItems.forEach((item) => {
        if (item.key === pathname && parentKey == null) {
            arr.push("");
        } else if (item.key === pathname && parentKey) {
            arr.push(parentKey);
        } else if (item.children && item.children.length > 0) {
            let temp = OpenKeys(item.children, pathname, item.key);
            if (temp.length >= 1) {
                arr = _.concat(arr, temp);
            }
        }
    });
    return arr;
}

export default () => {
    const location = useLocation();
    const history = useHistory();
    // 选中的item
    const [selected, setSelected] = useState<string[]>([]);
    // 监听的pathname变动
    const [pathName, setPathName] = useState<string>("");
    // open keys
    // const [openKeys, setOpenKeys] = useState<string[]>([]);

    history.listen((LocationVar: Location) => {
        if (LocationVar.pathname !== pathName) {
            setPathName(LocationVar.pathname);
        }
    });

    useEffect(
        function () {
            let temp: string;
            if (pathName) {
                temp = pathName;
            } else {
                temp = location.pathname;
            }
            // const findKeys = OpenKeys(MenuItem, temp);
            // const mergeArr = _.merge(openKeys, findKeys);
            // setOpenKeys(mergeArr);
            setSelected([temp]);
        },
        [pathName, location.pathname]
    );

    return (
        <Sider className={styles.siderMain}>
            <Menu
                mode='inline'
                style={{ height: "100%", borderRight: 0 }}
                forceSubMenuRender={true}
                // onOpenChange={(keys) => {
                //     setOpenKeys(keys as string[]);
                // }}
                // openKeys={openKeys}
                selectedKeys={selected}>
                {MenuItem.map((item) => {
                    if (item.children.length > 0) {
                        return NestedMenu(item);
                    } else {
                        return SingleMenu(item);
                    }
                })}
            </Menu>
        </Sider>
    );
};
