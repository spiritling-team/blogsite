import React, { useEffect, useReducer, useState } from "react";
import { Layout } from "antd";
import styles from "./layout.module.scss";
import HeaderNav from "./components/headerNav";
import SiderMenu from "./components/siderMenu";
import { RouteChildrenProps } from "interface/props";
import { useHistory } from "react-router-dom";

const { Content } = Layout;

const LayoutMain = (props: RouteChildrenProps) => {
    const history = useHistory();
    useEffect(function () {
        if (
            !localStorage.getItem("userInfo") &&
            !localStorage.getItem("token")
        ) {
            history.push(window._REACT_BASE_URL + "login");
        }
    });
    return (
        <Layout className={styles.layoutMain}>
            {/* 顶部菜单栏 */}
            <HeaderNav />
            <Layout className={styles.bottom}>
                {/* 侧边栏 */}
                <SiderMenu />
                <Layout>
                    {/* 内容content */}
                    <Content className={styles.content}>
                        {props.children}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    );
};

export default LayoutMain;
