const path = require('path');
const lessLocalIdentName = 'sl_[name]_[local]_[sha512:hash:base62:16]';

module.exports = {
    style: {
        modules: {
            localIdentName: lessLocalIdentName
        }
    },
    webpack: {
        alias: {
            '@global': path.resolve(__dirname, 'src/assets'),
        }
    },
    babel: {
        "plugins": [
            [
                "import",
                {
                    "libraryName": "@ant-design",
                    "libraryDirectory": "icons",
                    "camel2DashComponentName": false // default: true
                }
            ]
        ]
    }
}