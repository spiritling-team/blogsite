﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BlogSite.Entity.ArticleCategory
{
    public class ArticleCategoryCreatePostInput
    {
        [Required]
        [StringLength(20)]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [StringLength(30)]
        [JsonPropertyName("urlParam")]
        public string UrlParam { get; set; }

        [StringLength(255)]
        [JsonPropertyName("remark")]
        public string Remark { get; set; }
    }

    public class ArticleCategoryEditPutInput : ArticleCategoryCreatePostInput
    {
        [Required]
        [JsonPropertyName("id")]
        public int Id { get; set; }
    }
}
