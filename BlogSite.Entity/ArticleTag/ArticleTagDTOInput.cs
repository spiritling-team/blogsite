﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace BlogSite.Entity.ArticleTag
{
    public class ArticleTagCreatePostInput
    {
        [Required]
        [StringLength(20)]
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^#[\dA-Fa-f]{1,6}$")]
        [JsonPropertyName("bgColor")]
        public string BgColor { get; set; }

        [Required]
        [RegularExpression(@"^#[\dA-Fa-f]{1,6}$")]
        [JsonPropertyName("textColor")]
        public string TextColor { get; set; }
        
        [StringLength(30)]
        [JsonPropertyName("urlParam")]
        public string UrlParam { get; set; }

        [StringLength(255)]
        [JsonPropertyName("remark")]
        public string Remark { get; set; }
    }

    public class ArticleTagEditPutInput : ArticleTagCreatePostInput
    {
        [Required]
        [JsonPropertyName("id")]
        public int Id { get; set; }
    }
}
