﻿
namespace BlogSite.Entity.User
{
    public class JwtTokenOutput
    {
        /// <summary>
        /// 用户信息相关的存放到自定义header上
        /// </summary>
        public string UserKeyId { get; set; }
        /// <summary>
        /// 携带到验证header上
        /// </summary>
        public string JwtToken { get; set; }
        /// <summary>
        /// 当jwtToken过期后，才会使用这个请求获取新的JwtToken
        /// </summary>
        public string RefreshToken { get; set; }
    }
}
