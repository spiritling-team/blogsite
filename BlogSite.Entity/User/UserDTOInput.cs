﻿using System.Text.Json.Serialization;
using System;
using System.ComponentModel.DataAnnotations;

namespace BlogSite.Entity.User
{
    public class LoginInput
    {
        [Required]
        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [Required]
        [JsonPropertyName("passWord")]
        public string PassWord { get; set; }
    }

    public class RefreshTokenInput
    {
        [Required]
        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }

        [Required]
        [JsonPropertyName("keyId")]
        public string KeyId { get; set; }
    }
}
