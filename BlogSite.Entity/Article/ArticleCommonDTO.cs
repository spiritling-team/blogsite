﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSite.Entity.Article
{
    public class ArticleCommonDTO
    {
        public uint Id { get; set; }
        public uint ArticleCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime PublishStartTime { get; set; }
        public DateTime PublishEndTime { get; set; }
        public bool IsPublic { get; set; }
        public string UrlParam { get; set; }
        public string Remark { get; set; }
    }
    public  class ArticleDTO : ArticleCommonDTO
    {

    }
}
