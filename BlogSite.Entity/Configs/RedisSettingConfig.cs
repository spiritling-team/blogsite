﻿namespace BlogSite.Entity.Configs
{
    public class RedisSettingConfig
    {
        /// <summary>
        /// redis一次过期时间
        /// </summary>
        public int RedisAbsoluteExpirationSeconds { get; set; }

        /// <summary>
        /// redis 循环过期时间，如果访问就重置时间
        /// </summary>
        public int RedisSlidingExpirationSeconds { get; set; }
    }
}
