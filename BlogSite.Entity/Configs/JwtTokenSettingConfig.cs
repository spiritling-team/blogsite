﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSite.Entity.Configs
{
    public class JwtTokenSettingConfig
    {
        /// <summary>
        /// jwt 机密
        /// </summary>
        public string JwtTokenSecret { get; set; }

        /// <summary>
        /// jwt 发行人
        /// </summary>
        public string JwtTokenIssuer { get; set; }

        /// <summary>
        /// jwt 观众
        /// </summary>
        public string JwtTokenAudience { get; set; }

        /// <summary>
        /// jwt 过期时间
        /// </summary>
        public int JwtTokenAccessExpirationSeconds { get; set; }

        /// <summary>
        /// jwt 刷新过期时间
        /// </summary>
        public int JwtTokenSecretRefreshExpirationSeconds { get; set; }
    }
}
