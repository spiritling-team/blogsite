﻿# Using `Pomelo.EntityFrameworkCore.MySql`

```
dotnet ef dbcontext scaffold "Server=localhost;Database=blogsite;User=root;Password=***;TreatTinyAsBoolean=true;" "Pomelo.EntityFrameworkCore.MySql" --project BlogSite.Database -f -o Mysql2 -c BlogSiteDbContext
```

## Mysql 设置外键时对应.Net中的成果

> 一般设置为 CASCADE 类型，同步删除即可

### 多对多时

比如文章和标签就是多对多关系

则在.net中应该显示如下

```c#
public partial class Article
{
    public Article()
    {
        ArticleTagAbout = new HashSet<ArticleTagAbout>();
    }

    public virtual ICollection<ArticleTagAbout> ArticleTagAbout { get; set; }
}
```

```c#
public partial class ArticleTag
{
    public ArticleTag()
    {
        ArticleTagAbout = new HashSet<ArticleTagAbout>();
    }

    public virtual ICollection<ArticleTagAbout> ArticleTagAbout { get; set; }
}
```

```c#
public partial class ArticleTagAbout
{
    // 需要Id值，如果没有，好像无法自动生成实体时关联外键
    public uint Id { get; set; }
    public uint ArticleId { get; set; }
    public uint ArticleTagId { get; set; }

    public virtual Article Article { get; set; }
    public virtual ArticleTag ArticleTag { get; set; }
}
```

## 一对多

每个文章对应一个分类，**但是一个分类却对应多个文章**

```c#
public partial class ArticleCategory
{
    public ArticleCategory()
    {
        Article = new HashSet<Article>();
    }

    public virtual ICollection<Article> Article { get; set; }
}
```

```c#
public partial class Article
{

    public uint Id { get; set; }
    public uint ArticleCategoryId { get; set; }

    public virtual ArticleCategory ArticleCategory { get; set; }
}
```
