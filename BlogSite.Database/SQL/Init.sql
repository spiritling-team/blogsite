﻿CREATE TABLE IF NOT EXISTS `user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `passWord` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `refreshToken` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`, `keyId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ------------------------------------------------------------
-- TASK         ID : INIT
-- Descriptions    : 初始化创建用户表
-- ------------------------------------------------------------
-- ## BEGIN SQL

DELIMITER $$

CREATE PROCEDURE `user_init`()
BEGIN
  IF NOT EXISTS ( SELECT 1
                  FROM user
                  WHERE accountName = 'xp' or id=1 )
  THEN
      INSERT INTO `user` VALUES (1, '4a9783ad-2b04-4866-893d-8232c5ed88c0', 'xp', '3C9B9A6CBF1BDD362CEA6B0BF2EBEB420EBB85A1C8A75D538B5B2FEAA03B1186B33419D7DE38640964A2CB8BE5BD10805036EC1755DCAAE0C35A1675805383D7', '758a06f9-0cce-48e9-bec9-06c43d826ca0', 'aae91a67-dce2-44e5-982f-facd32d8223b', '2021-01-21 17:41:02', '2021-01-21 17:41:02');

END IF;

END$$

DELIMITER ;

-- 调用存储过程
CALL `user_init`();

-- 删除创建的存储过程
DROP PROCEDURE `user_init`;

-- ## END SQL


CREATE TABLE IF NOT EXISTS `articleCategory`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `urlParam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


CREATE TABLE IF NOT EXISTS `articleTag`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bgColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#000000',
  `textColor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#FFFFFF',
  `urlParam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;


CREATE TABLE IF NOT EXISTS `article`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `articleCategoryId` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `publishStartTime` datetime NOT NULL,
  `publishEndTime` datetime NOT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT 0,
  `urlParam` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_1`(`articleCategoryId`) USING BTREE,
  CONSTRAINT `article_1` FOREIGN KEY (`articleCategoryId`) REFERENCES `articleCategory` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;


CREATE TABLE IF NOT EXISTS `articleContent`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mdContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `htmlContent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `createdTime` datetime NOT NULL,
  `updatedTime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `articleRelevanceArticleContent`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `articleId` int(10) UNSIGNED NOT NULL,
  `articleContentId` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_content_re_1`(`articleId`) USING BTREE,
  INDEX `article_content_re_2`(`articleContentId`) USING BTREE,
  CONSTRAINT `article_content_re_1` FOREIGN KEY (`articleId`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `article_content_re_2` FOREIGN KEY (`articleContentId`) REFERENCES `articleContent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `articleRelevanceArticleTag`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `articleId` int(10) UNSIGNED NOT NULL,
  `articleTagId` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `article_tag_re_1`(`articleId`) USING BTREE,
  INDEX `article_tag_re_2`(`articleTagId`) USING BTREE,
  CONSTRAINT `article_tag_re_1` FOREIGN KEY (`articleId`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `article_tag_re_2` FOREIGN KEY (`articleTagId`) REFERENCES `articleTag` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;