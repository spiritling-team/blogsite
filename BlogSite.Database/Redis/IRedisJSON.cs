﻿namespace BlogSite.Database.Redis
{
    /// <summary>
    /// IRedisJSON 接口，其他的json类型或者其他类型都实现这个结构，用于后续静态方法扩展
    /// </summary>
    #pragma warning disable S101 // Types should be named in PascalCase
    public interface IRedisJSON
    #pragma warning restore S101 // Types should be named in PascalCase
    {
        // Interface intentionally left empty.
    }
}
