﻿using System.Text.Json;

namespace BlogSite.Database.Redis
{
    public static class RedisJSONExtension
    {
        /// <summary>
        /// 将实现于 IRedisJSON 接口的数据都可以转为字符串
        /// </summary>
        /// <returns></returns>
        public static byte[] Serialize<T>(this T redisJson) where T : IRedisJSON
        {
            return JsonSerializer.SerializeToUtf8Bytes(redisJson);
        }
        /// <summary>
        /// 反序列化RedisJson数据
        /// </summary>
        /// <typeparam name="T">接口IRedisJSON</typeparam>
        /// <returns></returns>
        public static T DeserializeRedisJson<T>(this string str) where T : IRedisJSON
        {
            return JsonSerializer.Deserialize<T>(str);
        }
    }
}
