﻿using System;

namespace BlogSite.Database.Redis
{
    public class UserRedis : IRedisJSON
    {
        public string UserKeyId { get; set; }

        public string RefreshToken { get; set; }
    }
}
