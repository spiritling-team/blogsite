﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class Article
    {
        public Article()
        {
            ArticleRelevanceArticleContent = new HashSet<ArticleRelevanceArticleContent>();
            ArticleRelevanceArticleTag = new HashSet<ArticleRelevanceArticleTag>();
        }

        public uint Id { get; set; }
        public string KeyId { get; set; }
        public uint ArticleCategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public DateTime PublishStartTime { get; set; }
        public DateTime PublishEndTime { get; set; }
        public bool IsPublic { get; set; }
        public string UrlParam { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ArticleCategory ArticleCategory { get; set; }
        public virtual ICollection<ArticleRelevanceArticleContent> ArticleRelevanceArticleContent { get; set; }
        public virtual ICollection<ArticleRelevanceArticleTag> ArticleRelevanceArticleTag { get; set; }
    }
}
