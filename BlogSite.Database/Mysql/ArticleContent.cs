﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class ArticleContent
    {
        public ArticleContent()
        {
            ArticleRelevanceArticleContent = new HashSet<ArticleRelevanceArticleContent>();
        }

        public uint Id { get; set; }
        public string MdContent { get; set; }
        public string HtmlContent { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ICollection<ArticleRelevanceArticleContent> ArticleRelevanceArticleContent { get; set; }
    }
}
