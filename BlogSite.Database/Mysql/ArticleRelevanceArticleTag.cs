﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class ArticleRelevanceArticleTag
    {
        public uint Id { get; set; }
        public uint ArticleId { get; set; }
        public uint ArticleTagId { get; set; }

        public virtual Article Article { get; set; }
        public virtual ArticleTag ArticleTag { get; set; }
    }
}
