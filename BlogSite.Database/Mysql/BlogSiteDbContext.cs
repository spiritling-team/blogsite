﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class BlogSiteDbContext : DbContext
    {
        public BlogSiteDbContext()
        {
        }

        public BlogSiteDbContext(DbContextOptions<BlogSiteDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategory { get; set; }
        public virtual DbSet<ArticleContent> ArticleContent { get; set; }
        public virtual DbSet<ArticleRelevanceArticleContent> ArticleRelevanceArticleContent { get; set; }
        public virtual DbSet<ArticleRelevanceArticleTag> ArticleRelevanceArticleTag { get; set; }
        public virtual DbSet<ArticleTag> ArticleTag { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;database=blogsite;user=root;password=xA123456;treattinyasboolean=true", x => x.ServerVersion("5.7.32-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>(entity =>
            {
                entity.ToTable("article");

                entity.HasIndex(e => e.ArticleCategoryId)
                    .HasName("article_1");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.ArticleCategoryId)
                    .HasColumnName("articleCategoryId")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Author)
                    .HasColumnName("author")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(500)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.IsPublic).HasColumnName("isPublic");

                entity.Property(e => e.KeyId)
                    .IsRequired()
                    .HasColumnName("keyId")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PublishEndTime)
                    .HasColumnName("publishEndTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PublishStartTime)
                    .HasColumnName("publishStartTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnName("updatedTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UrlParam)
                    .HasColumnName("urlParam")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.HasOne(d => d.ArticleCategory)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.ArticleCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("article_1");
            });

            modelBuilder.Entity<ArticleCategory>(entity =>
            {
                entity.ToTable("articleCategory");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.KeyId)
                    .IsRequired()
                    .HasColumnName("keyId")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnName("updatedTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UrlParam)
                    .HasColumnName("urlParam")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<ArticleContent>(entity =>
            {
                entity.ToTable("articleContent");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.HtmlContent)
                    .HasColumnName("htmlContent")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.MdContent)
                    .HasColumnName("mdContent")
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnName("updatedTime")
                    .HasColumnType("datetime")
                    .ValueGeneratedOnAddOrUpdate();
            });

            modelBuilder.Entity<ArticleRelevanceArticleContent>(entity =>
            {
                entity.ToTable("articleRelevanceArticleContent");

                entity.HasIndex(e => e.ArticleContentId)
                    .HasName("article_content_re_2");

                entity.HasIndex(e => e.ArticleId)
                    .HasName("article_content_re_1");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ArticleContentId)
                    .HasColumnName("articleContentId")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("articleId")
                    .HasColumnType("int(10) unsigned");

                entity.HasOne(d => d.ArticleContent)
                    .WithMany(p => p.ArticleRelevanceArticleContent)
                    .HasForeignKey(d => d.ArticleContentId)
                    .HasConstraintName("article_content_re_2");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleRelevanceArticleContent)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("article_content_re_1");
            });

            modelBuilder.Entity<ArticleRelevanceArticleTag>(entity =>
            {
                entity.ToTable("articleRelevanceArticleTag");

                entity.HasIndex(e => e.ArticleId)
                    .HasName("article_tag_re_1");

                entity.HasIndex(e => e.ArticleTagId)
                    .HasName("article_tag_re_2");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("articleId")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.ArticleTagId)
                    .HasColumnName("articleTagId")
                    .HasColumnType("int(10) unsigned");

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.ArticleRelevanceArticleTag)
                    .HasForeignKey(d => d.ArticleId)
                    .HasConstraintName("article_tag_re_1");

                entity.HasOne(d => d.ArticleTag)
                    .WithMany(p => p.ArticleRelevanceArticleTag)
                    .HasForeignKey(d => d.ArticleTagId)
                    .HasConstraintName("article_tag_re_2");
            });

            modelBuilder.Entity<ArticleTag>(entity =>
            {
                entity.ToTable("articleTag");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11) unsigned");

                entity.Property(e => e.BgColor)
                    .HasColumnName("bgColor")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'#000000'")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.KeyId)
                    .IsRequired()
                    .HasColumnName("keyId")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Remark)
                    .HasColumnName("remark")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.TextColor)
                    .HasColumnName("textColor")
                    .HasColumnType("varchar(255)")
                    .HasDefaultValueSql("'#FFFFFF'")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnName("updatedTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UrlParam)
                    .HasColumnName("urlParam")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.KeyId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("user");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(11) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.KeyId)
                    .HasColumnName("keyId")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CreatedTime)
                    .HasColumnName("createdTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.PassWord)
                    .IsRequired()
                    .HasColumnName("passWord")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.RefreshToken)
                    .HasColumnName("refreshToken")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnName("salt")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedTime)
                    .HasColumnName("updatedTime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("userName")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
