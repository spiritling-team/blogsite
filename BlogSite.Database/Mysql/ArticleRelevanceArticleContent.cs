﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class ArticleRelevanceArticleContent
    {
        public uint Id { get; set; }
        public uint ArticleId { get; set; }
        public uint ArticleContentId { get; set; }

        public virtual Article Article { get; set; }
        public virtual ArticleContent ArticleContent { get; set; }
    }
}
