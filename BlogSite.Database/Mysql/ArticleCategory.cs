﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class ArticleCategory
    {
        public ArticleCategory()
        {
            Article = new HashSet<Article>();
        }

        public uint Id { get; set; }
        public string KeyId { get; set; }
        public string Name { get; set; }
        public string UrlParam { get; set; }
        public string Remark { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }

        public virtual ICollection<Article> Article { get; set; }
    }
}
