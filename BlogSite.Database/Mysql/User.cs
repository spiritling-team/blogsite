﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace BlogSite.Database.Mysql
{
    public partial class User
    {
        public uint Id { get; set; }
        public string KeyId { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
        public string Salt { get; set; }
        public string RefreshToken { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime UpdatedTime { get; set; }
    }
}
