﻿using System;
using System.Text;

namespace BlogSite.CommonLib.Extension
{
    public static class RandomExtension
    {
        public static string DefaultString = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        public static string NextString(this Random random)
        {
            var len = random.Next();
            return NextStringRun(len);
        }

        public static string NextString(this Random random, int lens)
        {
            var len = random.Next(lens, lens);
            return NextStringRun(len);
        }

        public static string NextString(this Random random, int lens,string customeString)
        {
            var len = random.Next(lens, lens);
            return NextStringRun(len, customeString);
        }

        private static string NextStringRun(int len, string randomStr = null)
        {
            var chars = (randomStr.IsNullOrWhiteSpace() ? DefaultString : randomStr.Trim()).ToCharArray();
            var str = new StringBuilder(len);
            while (len > 0)
            {
                var r = new Random();
                str.Append(chars[r.Next(chars.Length - 1)]);
                len--;
            }
            return str.ToString();
        }
    }
}
