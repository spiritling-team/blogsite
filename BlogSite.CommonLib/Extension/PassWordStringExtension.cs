﻿using System;

namespace BlogSite.CommonLib.Extension
{
    public static class PassWordStringExtension
    {
        public static string EncryptUser(this string str,string guid)
        {
            var password = $"spirit{str}ling{guid}";
            return password.ConvertSHA512();
        }
    }
}
