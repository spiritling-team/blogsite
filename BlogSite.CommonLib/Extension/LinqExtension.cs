﻿using BlogSite.CommonLib.CommonEntity;
using BlogSite.CommonLib.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace BlogSite.CommonLib.Extension
{
    public static class LinqMethod
    {
        /// <summary>
        /// 使用自定linq扩展执行排序，查询，分页功能 item1: 未分页结果，item2：分页后的结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="coditionEntity"></param>
        /// <returns></returns>
        public static Tuple<IQueryable<T>, IQueryable<T>> UseCoditionFind<T>(this IQueryable<T> source, CoditionEntity coditionEntity)
        {
            var query = source;
            var resultQuery = source;
            if (coditionEntity.SearchKeyValue != null && coditionEntity.SearchKeyValue.Enable && !coditionEntity.SearchKeyValue.SearchContent.IsNullOrWhiteSpace())
            {
                query = query.Where(coditionEntity.SearchKeyValue.PropertyName, coditionEntity.SearchKeyValue.SearchContent);
            }
            if (coditionEntity.OrderByKeyValue != null && coditionEntity.OrderByKeyValue.Enable)
            {
                query = query.OrderBy(coditionEntity.OrderByKeyValue.PropertyName, coditionEntity.OrderByKeyValue.OrderDirection);
            }
            if (coditionEntity.PaginationKeyValue != null && coditionEntity.PaginationKeyValue.Enable)
            {
                resultQuery = query.Skip((coditionEntity.PaginationKeyValue.PageNumber - 1) * coditionEntity.PaginationKeyValue.PageSize)
                    .Take(coditionEntity.PaginationKeyValue.PageSize);
            }
            return new Tuple<IQueryable<T>, IQueryable<T>>(query, resultQuery);
        }
    }
    public static class LinqExtension
    {
        /// <summary>
        /// Orders the by.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, OrderDirection direction)
        {
            switch (direction)
            {
                case OrderDirection.ASC:
                    return source.OrderBy(ToLambda<T>(propertyName));

                case OrderDirection.DESC:
                    return source.OrderByDescending(ToLambda<T>(propertyName));

                default:
                    return source.OrderBy(ToLambda<T>(propertyName));
            }

        }

        /// <summary>
        /// Orders the by.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="direction">The direction.</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, int direction)
        {
            var dir = (OrderDirection)direction;

            switch (dir)
            {
                case OrderDirection.ASC:
                    return source.OrderBy(ToLambda<T>(propertyName));

                case OrderDirection.DESC:
                    return source.OrderByDescending(ToLambda<T>(propertyName));

                default:
                    return source.OrderBy(ToLambda<T>(propertyName));
            }

        }


        /// <summary>
        /// Orders the by.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName)
        {
            return source.OrderBy(ToLambda<T>(propertyName));
        }

        /// <summary>
        /// Orders the by descending.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string propertyName)
        {
            return source.OrderByDescending(ToLambda<T>(propertyName));
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> source, string propertyName, string content)
        {
            return source.Where(ToLambdaWhere<T>(propertyName, content));
        }

        public static IQueryable<T> WhereList<T>(this IQueryable<T> source, string propertyName, List<string> contentList)
        {
            return source.Where(ToLambdaWhere<T>(propertyName, contentList));
        }

        /// <summary>
        /// Where Lambda 表示式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <param name="contentList"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> ToLambdaWhere<T>(string propertyName, List<string> contentList)
        {
            // 传递到表达式目录树的参数 x 和它的类型
            ParameterExpression x = Expression.Parameter(typeof(T));

            MemberExpression property = Expression.Property(x, propertyName);
            ConstantExpression constant = Expression.Constant(contentList, typeof(List<string>));

            var methodCall = Expression.Call(constant, "Contains", Type.EmptyTypes, property);


            var expression = Expression.Lambda<Func<T, bool>>(methodCall, new ParameterExpression[] { x });
            return expression;
        }

        /// <summary>
        /// Where Lambda 表示式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        private static Expression<Func<T, bool>> ToLambdaWhere<T>(string propertyName, string content)
        {
            // 传递到表达式目录树的参数 x 和它的类型
            ParameterExpression x = Expression.Parameter(typeof(T));
            // 获取类型的属性
            PropertyInfo prop = typeof(T).GetProperty(propertyName);
            // 设定常量值相当于Contains中传递的值
            ConstantExpression constant = Expression.Constant(content, typeof(string));
            // 使用类型调用对应的属性
            var propExp = Expression.Property(x, prop);
            // 获取方法
            MethodInfo contains = typeof(string).GetMethod("Contains", new Type[] { });
            // 给属性调用上面获取的方法，传递值
            var methodCall = Expression.Call(propExp, contains, new Expression[] { constant });
            // 拼装成表达式目录树
            Expression<Func<T, bool>> expression = Expression.Lambda<Func<T, bool>>(methodCall, new ParameterExpression[] { x });
            return expression;
        }

        /// <summary>
        /// Converts to lambda.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        private static Expression<Func<T, object>> ToLambda<T>(string propertyName)
        {
            // 传递到表达式目录树的参数 x
            ParameterExpression x = Expression.Parameter(typeof(T));
            // 通过传递过来的属性字符串获取对应的属性
            MemberExpression property = Expression.Property(x, propertyName);
            // 创建一个表示类型转换运算的 UnaryExpression。
            // 使用 property.Type  会在最后转换时出现错误
            var propAsObject = Expression.Convert(property, typeof(object));

            Expression<Func<T, object>> expression = Expression.Lambda<Func<T, object>>(propAsObject, x);
            return expression;
        }
    }
}
