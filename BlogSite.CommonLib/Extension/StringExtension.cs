﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BlogSite.CommonLib
{
    public static class StringExtension
    {
        /// <summary>
        /// 将字符串进行SHA512加密
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string ConvertSHA512(this string s)
        {
            var bytes = Encoding.UTF8.GetBytes(s);
            using (var hash = SHA512.Create())
            {
                var hashedInputBytes = hash.ComputeHash(bytes);

                // Convert to text
                // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
                var hashedInputStringBuilder = new StringBuilder(128);
                foreach (var b in hashedInputBytes)
                    hashedInputStringBuilder.Append(b.ToString("X2"));
                return hashedInputStringBuilder.ToString();
            }
        }
        public static bool IsNullOrWhiteSpace(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }
        /// <summary>
        /// 首字母大写
        /// </summary>
        public static string ToUpperFirstLetter(this string value)
        {
            if (value.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            letters[0] = char.ToUpper(letters[0]);

            return new string(letters);
        }
        /// <summary>
        /// 首字母小写
        /// </summary>
        public static string ToLowerFirstLetter(this string value)
        {
            if (value.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            letters[0] = char.ToLower(letters[0]);

            return new string(letters);
        }
        /// <summary>
        /// 字符串转大写
        /// </summary>
        public static string ToLower(this string value)
        {
            if (value.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }

            char[] letters = value.ToCharArray();
            for (int i = 0; i < letters.Length; i++)
            {
                letters[i] = char.ToLower(letters[i]);
            }

            return new string(letters);
        }
    }
}
