﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace BlogSite.CommonLib
{
    /// <summary>
    /// Enum 静态扩展方法
    /// </summary>
    public static class EnumExtension
    {
        /// <summary>
        /// 获取枚举值上的Description特性的说明
        /// </summary>
        /// <returns>特性的说明</returns>
        public static string GetDescription(this Enum en)
        {
            var type = en.GetType();
            FieldInfo field = type.GetField(Enum.GetName(type, en));
            if (!(Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute descAttr))
            {
                return string.Empty;
            }

            return descAttr.Description;
        }
    }
}
