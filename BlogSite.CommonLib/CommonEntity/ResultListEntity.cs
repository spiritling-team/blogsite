﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogSite.CommonLib.CommonEntity
{
    public class ResultListEntity<T>
    {
        public CoditionEntity CoditionEntity { get; set; }
        public List<T> Data { get; set; }
    }
}
