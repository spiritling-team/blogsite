﻿using BlogSite.CommonLib.Enums;
using System.Text.Json.Serialization;

namespace BlogSite.CommonLib.CommonEntity
{
    /// <summary>
    /// 查询基本实体
    /// </summary>
    public class CoditionEntity
    {
        [JsonPropertyName("paginationKeyValue")]
        public PaginationKeyValue PaginationKeyValue { get; set; }

        [JsonPropertyName("orderByKeyValue")]
        public OrderByKeyValue OrderByKeyValue { get; set; }

        [JsonPropertyName("searchKeyValue")]
        public SearchKeyValue SearchKeyValue { get; set; }
    }
    public class SearchKeyValue
    {
        private string _propertyName;
        [JsonPropertyName("enable")]
        public bool Enable { get; set; }
        [JsonPropertyName("propertyName")]
        public string PropertyName
        {
            get
            {
                return _propertyName.ToUpperFirstLetter();
            }
            set
            {
                _propertyName = value;
            }
        }
        [JsonPropertyName("searchContent")]
        public string SearchContent { get; set; }
    }

    /// <summary>
    /// 排序字段格式
    /// </summary>
    public class OrderByKeyValue
    {
        private string _propertyName;
        [JsonPropertyName("enable")]
        public bool Enable { get; set; }
        [JsonPropertyName("propertyName")]
        public string PropertyName {
            get
            {
                return _propertyName.ToUpperFirstLetter();
            }
            set
            {
                _propertyName = value;
            }
        }
        [JsonPropertyName("orderDirection")]
        public OrderDirection OrderDirection { get; set; }
    }
    /// <summary>
    /// 分页格式
    /// </summary>
    public class PaginationKeyValue
    {
        [JsonPropertyName("enable")]
        public bool Enable { get; set; }
        [JsonPropertyName("count")]
        public int Count { get; set; }
        [JsonPropertyName("pageSize")]
        public int PageSize { get; set; }
        [JsonPropertyName("pageNumber")]
        public int PageNumber { get; set; }
    }
}
