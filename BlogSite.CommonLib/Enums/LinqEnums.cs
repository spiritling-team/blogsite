﻿using System.ComponentModel;

namespace BlogSite.CommonLib.Enums
{
    /// <summary>
    /// 排序类型
    /// </summary>
    public enum OrderDirection
    {
        /// <summary>
        /// 升序
        /// </summary>
        [Description("升序")]
        ASC = 0,
        /// <summary>
        /// 降序
        /// </summary>
        [Description("降序")]
        DESC = 1
    }
}
